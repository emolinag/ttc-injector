project = ./src/example_designs/altera_s10/alt_sx_devkit/q_project
project_name = ttc-injector
project_rev = ttc-injector
src = ./src
example_design = $(src)/example_designs
##file_src = $(wildcard $(src)/*.vhd)
file_src = $(shell find $(example_design) -name "*.vhd")
test = $(wildcard ./sim/libraries/*)
testlibs = $(test:./sim/libraries/%=-L %)


files = ./$(src)/gbt_bank/altera_s10/gbt_rx/alt_sx_rx_dpram/alt_sx_rx_dpram/sim/alt_sx_rx_dpram.vhd ./$(src)/gbt_bank/altera_s10/gbt_tx/alt_sx_tx_dpram/alt_sx_tx_dpram/sim/alt_sx_tx_dpram.vhd ./$(src)/gbt_bank/altera_s10/mgt/gx_reset_tx/gx_reset_tx/sim/gx_reset_tx.vhd ./$(src)/gbt_bank/altera_s10/mgt/tx_pll_rst/mgt_tx_pll_rst/sim/mgt_tx_pll_rst.vhd ./$(src)/gbt_bank/altera_s10/mgt/gx_reset_rx/gx_reset_rx/sim/gx_reset_rx.vhd ./$(src)/gbt_bank/altera_s10/mgt/gx_x2/gx_x2/sim/gx_x2.vhd  ./$(src)/gbt_bank/altera_s10/mgt/gx_x1/gx_x1/sim/gx_x1.vhd ./$(src)/gbt_bank/altera_s10/mgt/fpll_pll/mgt_fpll/sim/mgt_fpll.vhd ./$(src)/gbt_bank/altera_s10/mgt/atx_pll/mgt_atxpll/sim/mgt_atxpll.vhd          

compile: quartus_asm quartus_fit quartus_syn 

./$(src)/gbt_bank/altera_s10/gbt_rx/alt_sx_rx_dpram/alt_sx_rx_dpram/sim/alt_sx_rx_dpram.vhd: ./$(src)/gbt_bank/altera_s10/gbt_rx/alt_sx_rx_dpram/alt_sx_rx_dpram.ip
	rm -rf $(patsubst %.ip,%, $^)
	$(QSYS_ROOTDIR)/../../qsys/bin/qsys-generate $^  --quartus-project=$(project)/$(project_name) --rev=$(project_rev) --simulation=VHDL --synthesis=vhdl --allow-mixed-language-simulation --output-directory=$(patsubst %.ip,%, $^) -family="Stratix 10" --part=1SG280HU2F50E2VG


./$(src)/gbt_bank/altera_s10/gbt_tx/alt_sx_tx_dpram/alt_sx_tx_dpram/sim/alt_sx_tx_dpram.vhd: ./$(src)/gbt_bank/altera_s10/gbt_tx/alt_sx_tx_dpram/alt_sx_tx_dpram.ip
	rm -rf $(patsubst %.ip,%, $^)
	$(QSYS_ROOTDIR)/../../qsys/bin/qsys-generate $^  --quartus-project=$(project)/$(project_name) --rev=$(project_rev) --simulation=VHDL --synthesis=vhdl --allow-mixed-language-simulation --output-directory=$(patsubst %.ip,%, $^) -family="Stratix 10" --part=1SG280HU2F50E2VG


./$(src)/gbt_bank/altera_s10/mgt/gx_reset_tx/gx_reset_tx/sim/gx_reset_tx.vhd: ./$(src)/gbt_bank/altera_s10/mgt/gx_reset_tx/gx_reset_tx.ip
	rm -rf $(patsubst %.ip,%, $^)
	$(QSYS_ROOTDIR)/../../qsys/bin/qsys-generate $^  --quartus-project=$(project)/$(project_name) --rev=$(project_rev) --simulation=VHDL --synthesis=vhdl --allow-mixed-language-simulation --output-directory=$(patsubst %.ip,%, $^) -family="Stratix 10" --part=1SG280HU2F50E2VG

./$(src)/gbt_bank/altera_s10/mgt/tx_pll_rst/mgt_tx_pll_rst/sim/mgt_tx_pll_rst.vhd: ./$(src)/gbt_bank/altera_s10/mgt/tx_pll_rst/mgt_tx_pll_rst.ip
	rm -rf $(patsubst %.ip,%, $^)
	$(QSYS_ROOTDIR)/../../qsys/bin/qsys-generate $^  --quartus-project=$(project)/$(project_name) --rev=$(project_rev) --simulation=VHDL --synthesis=vhdl --allow-mixed-language-simulation --output-directory=$(patsubst %.ip,%, $^) -family="Stratix 10" --part=1SG280HU2F50E2VG


./$(src)/gbt_bank/altera_s10/mgt/gx_reset_rx/gx_reset_rx/sim/gx_reset_rx.vhd: ./$(src)/gbt_bank/altera_s10/mgt/gx_reset_rx/gx_reset_rx.ip
	rm -rf $(patsubst %.ip,%, $^)
	$(QSYS_ROOTDIR)/../../qsys/bin/qsys-generate $^  --quartus-project=$(project)/$(project_name) --rev=$(project_rev) --simulation=VHDL --synthesis=vhdl --allow-mixed-language-simulation --output-directory=$(patsubst %.ip,%, $^) -family="Stratix 10" --part=1SG280HU2F50E2VG


./$(src)/gbt_bank/altera_s10/mgt/gx_x2/gx_x2/sim/gx_x2.vhd: ./$(src)/gbt_bank/altera_s10/mgt/gx_x2/gx_x2.ip
	rm -rf $(patsubst %.ip,%, $^)
	$(QSYS_ROOTDIR)/../../qsys/bin/qsys-generate $^  --quartus-project=$(project)/$(project_name) --rev=$(project_rev) --simulation=VHDL --synthesis=vhdl --allow-mixed-language-simulation --output-directory=$(patsubst %.ip,%, $^) -family="Stratix 10" --part=1SG280HU2F50E2VG


./$(src)/gbt_bank/altera_s10/mgt/gx_x1/gx_x1/sim/gx_x1.vhd: ./$(src)/gbt_bank/altera_s10/mgt/gx_x1/gx_x1.ip
	rm -rf $(patsubst %.ip,%, $^)
	$(QSYS_ROOTDIR)/../../qsys/bin/qsys-generate $^  --quartus-project=$(project)/$(project_name) --rev=$(project_rev) --simulation=VHDL --synthesis=vhdl --allow-mixed-language-simulation --output-directory=$(patsubst %.ip,%, $^) -family="Stratix 10" --part=1SG280HU2F50E2VG


./$(src)/gbt_bank/altera_s10/mgt/fpll_pll/mgt_fpll/sim/mgt_fpll.vhd: ./$(src)/gbt_bank/altera_s10/mgt/fpll_pll/mgt_fpll.ip
	rm -rf $(patsubst %.ip,%, $^)
	$(QSYS_ROOTDIR)/../../qsys/bin/qsys-generate $^  --quartus-project=$(project)/$(project_name) --rev=$(project_rev) --simulation=VHDL --synthesis=vhdl --allow-mixed-language-simulation --output-directory=$(patsubst %.ip,%, $^) -family="Stratix 10" --part=1SG280HU2F50E2VG

./$(src)/gbt_bank/altera_s10/mgt/atx_pll/mgt_atxpll/sim/mgt_atxpll.vhd: ./$(src)/gbt_bank/altera_s10/mgt/atx_pll/mgt_atxpll.ip
	rm -rf $(patsubst %.ip,%, $^)
	$(QSYS_ROOTDIR)/../../qsys/bin/qsys-generate $^  --quartus-project=$(project)/$(project_name) --rev=$(project_rev) --simulation=VHDL --synthesis=vhdl --allow-mixed-language-simulation --output-directory=$(patsubst %.ip,%, $^) -family="Stratix 10" --part=1SG280HU2F50E2VG


##./$(src)/issp/synth/issp.vhd : ./$(src)/issp.ip
##	rm -rf $(patsubst %.ip,%, $^)
##	$(QSYS_ROOTDIR)/../../qsys/bin/qsys-generate $^  --quartus-project=$(project)/$(project_name) --rev=$(project_rev) --simulation=VHDL --synthesis=vhdl --allow-mixed-language-simulation --output-directory=$(patsubst %.ip,%, $^) -family="Stratix 10" --part=1SG280HU2F50E2VG
##


$(project)/output_$(project_rev)/$(project_rev).syn.summary: $(file_src) $(files) $(project)/one_channel.stp $(project)/$(project_rev).qsf 
	quartus_syn $(project)/$(project_name) -c $(project_rev)

quartus_syn: $(project)/output_$(project_rev)/$(project_rev).syn.summary  
#	quartus_syn $(project)/$(project_name) -c $(project_rev)

$(project)/output_$(project_rev)/$(project_rev).fit.summary: $(project)/output_$(project_rev)/$(project_rev).syn.summary
	quartus_fit $(project)/$(project_name) -c $(project_rev) --plan --place --route --retime --finalize

quartus_fit: $(project)/output_$(project_rev)/$(project_rev).fit.summary


$(project)/output_$(project_rev)/$(project_rev).sof: $(project)/output_$(project_rev)/$(project_rev).fit.summary
	quartus_asm $(project)/$(project_name) -c $(project_rev)

quartus_asm: $(project)/output_$(project_rev)/$(project_rev).sof 


./sim/simulation/mentor/msim_setup.tcl :  $(files) 
	ip-setup-simulation --quartus-project=$(project)/$(project_rev) --revision=$(project_rev) --output-directory=./sim/simulation --use-relative-paths 

#quartuslib : ./sim/simulation/mentor/msim_setup.tcl 
./libraries/_info : ./sim/simulation/mentor/msim_setup.tcl 
	rm -rf ./libraries
	vsim -c -do sim/quartuslib.do
	#echo "all is compiled" 

gbt_bank : ./src/gbt-fpga/gbt_bank/core_sources/gbt_bank_package.vhd ./libraries/_info

	vsim -c -do "set sources "src"; do sim/gbt_bank.do; quit"

simu: gbt_bank 
	@echo "lauch vsim"
	vsim -c -do sim/mydo.do
	vsim -c  -t fs -novopt -L work -L work_lib -L altera_ver -L lpm_ver -L sgate_ver -L altera_mf_ver -L altera_lnsim_ver -L fourteennm_ver -L fourteennm_ct1_ver -L altera -L lpm -L sgate -L altera_mf -L altera_lnsim -L fourteennm -L fourteennm_ct1   $(testlibs) ttc_injector_tb -do "do wave.do; run 450us" 


cleanall:
	rm -rf $(project)/output_$(project_rev)
	quartus_ipgenerate  $(project)/$(project_name) -c $(project_rev) --clean

#loopback_test.cdf requires that ttc-injector-issp.sof shoul be in the path <root>/q_project/ttc-injector-issp.sof
#program:

