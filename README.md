## ttc-injector project

the project is still ongoing.

## Quick hands-on

To clone and after simulate the follow steps to be done:

* Clone repository.
* Checkout a local branch (Use the **master**).
* Open the ttc-injector project file with quartus.
* Generate IPs.
* Generate simulation script for Altera IPs.
* Run the mydo.do file with modelsim (It runs the reset sequence).



```
**If older version of git** use the `--recursive` instead `--recurse-submodules`

## Makefile for simulation

Now a makefile in ./sim folder can be found. The command below can be done:

```bash
make simu
```

It launch the simulation reset checks

## Makefile for compilation

Now a makefile can be found in the root of project. The command below can be done:

```bash
make compile
```

At the end of execution the ttc-injectot-issp.sof file is created to be used in stratix10 devkit
look at the followig folder:

`src/example_designs/altera_s10/alt_sx_devkit/q_project/`

