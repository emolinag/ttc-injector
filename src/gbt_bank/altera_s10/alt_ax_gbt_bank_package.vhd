-------------------------------------------------------
--! @file
--! @author Julian Mendez <julian.mendez@cern.ch> (CERN - EP-ESE-BE)
--! @version 6.0
--! @brief GBT-FPGA IP - Device specific package
-------------------------------------------------------

--! IEEE VHDL standard library:
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

--! @brief vendor_specific_gbt_bank_package - Device specific parameter (Package)
--! @details 
--! The vendor_specific_gbt_bank_package package contains the constant values used to configure the device specific parameters
--! and the record used to connect the transceiver's signals, which are device specific
package vendor_specific_gbt_bank_package is
   
   --=================================== GBT Bank setup ==================================--
	
	-- Device dependant configuration (Modifications are not recommended)
	constant MAX_NUM_GBT_LINK                    : integer := 6;            --! Maximum number of links per bank
   
   constant ATXPLL								      : integer := 0;            --! ATXPLL constant definition
   constant FPLL								         : integer := 1;            --! FPLL constant definition
	
   constant XCVR_TX_PLL						         : integer := ATXPLL;       --! Select the PLL to use for the transceiver
	constant GBTRX_BITSLIP_MIN_DLY               : integer := 100;          --! Minimum number of clock cycle to wait for a bitslip action
   constant GBTRX_BITSLIP_MGT_RX_RESET_DELAY    : integer := 25e3;         --! Minimum number of clock cycle to wait for an MGT reset action	
   constant GBTTX_MGTREG_DEEP					      : integer := 5;            --! Number of tx flip-flops to be implemented in order to solve timming issue: depends on the number of links.
   
	-- PCS WordSize dependant
   constant WORD_WIDTH                          : integer := 20;           --! MGT word size (20 [240MHz] / 40 [120MHz])
   constant GBT_WORD_RATIO						      : integer := 6;            --! Size ratio between GBT-Frame word (120bit) and MGT word size (120/WORD_WIDTH = 6 [240MHz] / 3 [120MHz])
   constant GBTRX_BITSLIP_NBR_MAX               : integer := 19;           --! Maximum number of bitslip before going back to the first position (WORD_WIDTH-1)
   constant GBT_GEARBOXWORDADDR_SIZE            : integer := 6;            --! Size in bit of the ram address used for the gearbox [Std] : Log2((120 * 8)/WORD_WIDTH)	
	
   constant RX_GEARBOXSYNCSHIFT_COUNT           : integer := 4;            --! Number of clock cycle between the Rx gearbox and the Descrambler (multicyle to allow word decoding in more than one clock cycle). This constant shall be used to fix the multicycle constraint
	
   --=====================================================================================--
   
   --================================ Record Declarations ================================--   
   
   --! Records to define device specific signals connected to the transceiver
   type mgtDeviceSpecific_i_R is
   record
		
		-- Reconfigurator
		reconf_reset									: std_logic;
		reconf_clk										: std_logic;
		reconf_avmm_addr								: std_logic_vector(11 downto 0);
		reconf_avmm_read								: std_logic;
		reconf_avmm_write								: std_logic;
		reconf_avmm_writedata						: std_logic_vector(31 downto 0);
		
		
		-- TX Reconfigurator
		txpll_reconf_reset							: std_logic;
		txpll_reconf_clk								: std_logic;
		txpll_reconf_avmm_addr						: std_logic_vector(10 downto 0);
		txpll_reconf_avmm_read						: std_logic;
		txpll_reconf_avmm_write						: std_logic;
		txpll_reconf_avmm_writedata				: std_logic_vector(31 downto 0);
		
		rxSerialData									: std_logic_vector(1 to MAX_NUM_GBT_LINK);
		loopBack										   : std_logic_vector(1 to MAX_NUM_GBT_LINK);
		tx_polarity                            : std_logic_vector(1 to MAX_NUM_GBT_LINK);
		rx_polarity                            : std_logic_vector(1 to MAX_NUM_GBT_LINK);

	end record;
   
   type mgtDeviceSpecific_o_R is
   record
	
		-- Reconfigurator
		reconf_avmm_readdata							: std_logic_vector(31 downto 0);
		reconf_avmm_waitrequest						: std_logic;
			
		-- TX PLL Reconfigurator
		txpll_reconf_avmm_readdata					: std_logic_vector(31 downto 0);
		txpll_reconf_avmm_waitrequest				: std_logic;
		
		
		txSerialData									: std_logic_vector(1 to MAX_NUM_GBT_LINK);
		
   end record;
   
   --=====================================================================================--    
end vendor_specific_gbt_bank_package;   
--=================================================================================================--
--#################################################################################################--
--=================================================================================================--
