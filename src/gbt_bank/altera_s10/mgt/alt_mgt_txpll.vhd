-------------------------------------------------------
--! @file
--! @author Julian Mendez <julian.mendez@cern.ch> (CERN - EP-ESE-BE)
--! @version 6.0
--! @brief GBT-FPGA IP - Transceiver Tx PLL
-------------------------------------------------------

-- IEEE VHDL standard library:
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

--! Altera devices library:
library altera; 
library altera_mf;
library lpm;
use altera.altera_primitives_components.all;   
use altera_mf.altera_mf_components.all;
use lpm.lpm_components.all;

--! Custom libraries and packages:
use work.gbt_bank_package.all;
use work.vendor_specific_gbt_bank_package.all;

--! Libraries for direct instantiation:
library mgt_atxpll;
library mgt_tx_pll_rst;

--! @brief ALT_ax_mgt_txpll - Transceiver Tx PLL
--! @details 
--! The ALT_ax_mgt_txpll provides the serial clock to the transmitter
entity alt_mgt_txpll is
   port (      
      
      --=======--  
      -- Reset --  
      --=======--  
      RESET_I                                   : in  std_logic;
      
      --===============--  
      -- Clocks scheme --  
      --===============--        
      MGT_REFCLK_I                              : in  std_logic;
      TX_SERIAL_CLK                             : out std_logic;
      TX_BONDING_CLK_O									: out std_logic_vector(5 downto 0);
		      
      --=========--
      -- Control --
      --=========--      
      LOCKED_O                                  : out std_logic;
		
		--============--
		-- Reconfig.  --
		--============--
		tx_pll_reconfig_write       					: in  std_logic                     := '0';             --    reconfig_avmm0.write
		tx_pll_reconfig_read        					: in  std_logic                     := '0';             --                  .read
		tx_pll_reconfig_address     					: in  std_logic_vector(10 downto 0)  := (others => '0'); --                  .address
		tx_pll_reconfig_writedata   					: in  std_logic_vector(31 downto 0) := (others => '0'); --                  .writedata
		tx_pll_reconfig_readdata    					: out std_logic_vector(31 downto 0);                    --                  .readdata
		tx_pll_reconfig_waitrequest 					: out std_logic;                                        --                  .waitrequest
		tx_pll_reconfig_clk         					: in  std_logic                     := '0';             --     reconfig_clk0.clk
		tx_pll_reconfig_reset       					: in  std_logic                     := '0'              --   reconfig_reset0.reset

   );
end alt_mgt_txpll;

