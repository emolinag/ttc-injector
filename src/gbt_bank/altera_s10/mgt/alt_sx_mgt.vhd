-------------------------------------------------------
--! @file
--! @author Julian Mendez <julian.mendez@cern.ch> (CERN - EP-ESE-BE)
--! @version 6.0
--! @brief GBT-FPGA IP - Device specific transceiver
-------------------------------------------------------

--! IEEE VHDL standard library:
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

--! Altera devices library:
library altera;
library altera_mf;
library lpm;
use altera.altera_primitives_components.all;
use altera_mf.altera_mf_components.all;
use lpm.lpm_components.all;

--! Custom libraries and packages:
use work.gbt_bank_package.all;
use work.vendor_specific_gbt_bank_package.all;

--! Libraries for direct instantiation:
library gx_x2;
library gx_x1;

--! @brief MGT - Transceiver
--! @details
--! The MGT module provides the interface to the transceivers to send the GBT-links via
--! high speed links (@4.8Gbps)
entity mgt is
   generic (
        NUM_LINKS                    : integer := 2
   );
   port (

        --=============--
        -- Clocks      --
        --=============--
        MGT_REFCLK_i                 : in  std_logic;

        MGT_RXUSRCLK_o               : out std_logic_vector(1 to NUM_LINKS);
        MGT_TXUSRCLK_o               : out std_logic_vector(1 to NUM_LINKS);

        --=============--
        -- Resets      --
        --=============--
        MGT_TXRESET_i                : in  std_logic_vector(1 to NUM_LINKS);
        MGT_RXRESET_i                : in  std_logic_vector(1 to NUM_LINKS);

        --=============--
        -- Status      --
        --=============--
        MGT_TXREADY_o                : out std_logic_vector(1 to NUM_LINKS);
        MGT_RXREADY_o                : out std_logic_vector(1 to NUM_LINKS);

        RX_HEADERLOCKED_o            : out std_logic_vector(1 to NUM_LINKS);
        RX_HEADERFLAG_o              : out std_logic_vector(1 to NUM_LINKS);
		  MGT_RSTCNT_o                 : out gbt_reg8_A(1 to NUM_LINKS);

		  --==============--
		  -- Control      --
		  --==============--
		  MGT_AUTORSTEn_i              : in  std_logic_vector(1 to NUM_LINKS);
        MGT_AUTORSTONEVEN_i          : in  std_logic_vector(1 to NUM_LINKS);

        --==============--
        -- Data         --
        --==============--
        MGT_USRWORD_i                : in  word_mxnbit_A(1 to NUM_LINKS);
        MGT_USRWORD_o                : out word_mxnbit_A(1 to NUM_LINKS);

        --=============================--
        -- Device specific connections --
        --=============================--
        MGT_DEVSPEC_i                : in  mgtDeviceSpecific_i_R;
        MGT_DEVSPEC_o                : out mgtDeviceSpecific_o_R;
--------------debug--------------------
        TX_ANALOGRESET_DEBUG        : out std_logic_vector(1 to NUM_LINKS);
        TX_DIGITALRESET_DEBUG       : out std_logic_vector(1 to NUM_LINKS);
        TX_READY_DEBUG              : out std_logic_vector(1 to NUM_LINKS);
        RX_ANALOGRESET_DEBUG        : out std_logic_vector(1 to NUM_LINKS);
        RX_DIGITALRESET_DEBUG       : out std_logic_vector(1 to NUM_LINKS);
        RX_READY_DEBUG              : out std_logic_vector(1 to NUM_LINKS);
        RX_CAL_BUSY                 : out std_logic_vector(1 to NUM_LINKS);
        RX_IS_LOCKEDTODATA          : out std_logic_vector(1 to NUM_LINKS)

   );
end mgt;

--! @brief MGT - Transceiver
--! @details The MGT module implements all the logic required to send the GBT frame on high speed
--! links: resets modules for the transceiver, Tx PLL and alignement logic to align the received word with the
--! GBT frame header.
architecture structural of mgt is

    --================================ Signal Declarations ================================--

    --======================--
    -- GX reset controllers --
    --======================--

    signal txAnalogReset_from_gxRstCtrl             : std_logic_vector           (1 to NUM_LINKS);
    signal txDigitalReset_from_gxRstCtrl            : std_logic_vector           (1 to NUM_LINKS);
    signal txCalBusy_to_gxRstCtrl                   : std_logic_vector           (1 to NUM_LINKS);
    signal txReady_from_gxRstCtrl                   : std_logic_vector           (1 to NUM_LINKS);
    ------------------------------------------------
    signal rxAnalogreset_from_gxRstCtrl             : std_logic_vector           (1 to NUM_LINKS);
    signal rxDigitalreset_from_gxRstCtrl            : std_logic_vector           (1 to NUM_LINKS);
    signal rxCalBusy_to_gxRstCtrl                   : std_logic_vector           (1 to NUM_LINKS);
    signal rxReady_from_gxRstCtrl                   : std_logic_vector           (1 to NUM_LINKS);

    --=======================--
    -- TX_WORDCLK monitoring --
    --=======================--
    signal reset_mgt_tx                                        : std_logic_vector           (1 to NUM_LINKS);

    --====================--
    -- RX phase alignment --
    --====================--
    signal run_to_rxBitSlipControl                  : std_logic_vector           (1 to NUM_LINKS);
    signal rxBitSlip_from_rxBitSlipControl          : std_logic_vector           (1 to NUM_LINKS);
    signal rxBitSlip_to_gxLatOpt                    : std_logic_vector           (1 to NUM_LINKS);
    signal done_from_rxBitSlipControl               : std_logic_vector           (1 to NUM_LINKS);

    signal resetGxRx_from_rxBitSlipControl          : std_logic_vector           (1 to NUM_LINKS);
    signal resetGxTx_from_rxBitSlipControl          : std_logic_vector           (1 to NUM_LINKS);

    type rstBitSlip_FSM_t                 is (idle, reset_tx, reset_rx);
    signal mgtRst_from_bitslipCtrl       : std_logic_vector(1 to NUM_LINKS);
    signal mgtRst_from_bitslipCtrl_r0     : std_logic_vector(1 to NUM_LINKS);

    type rstBitSlip_FSM_t_A              is array (natural range <>) of rstBitSlip_FSM_t;
    signal rstBitSlip_FSM                : rstBitSlip_FSM_t_A(1 to NUM_LINKS);

    --=========--
    -- ATX PLL --
    --=========--
    signal tx_bonding_clocks                                : std_logic_vector              (5 downto 0);
    signal TXPLLLocked                                        : std_logic;

    --================================================--
    -- Multi-Gigabit Transceivers (latency-optimized) --
    --================================================--
    signal tx_analogreset_stat_s            : std_logic_vector           (1 to NUM_LINKS);
    signal rx_analogreset_stat_s            : std_logic_vector           (1 to NUM_LINKS);
    signal tx_digitalreset_stat_s            : std_logic_vector           (1 to NUM_LINKS);
    signal rx_digitalreset_stat_s            : std_logic_vector           (1 to NUM_LINKS);
    signal rxIsLockedToData_from_gxLatOpt           : std_logic_vector           (1 to NUM_LINKS);
    signal txCalBusy_from_gxLatOpt                  : std_logic_vector           (1 to NUM_LINKS);
    signal rxCalBusy_from_gxLatOpt                  : std_logic_vector           (1 to NUM_LINKS);

    signal reconfToXCVR                                        : std_logic_vector              ((NUM_LINKS*70)-1 downto 0);
    signal XCVRToReconf                                        : std_logic_vector              ((NUM_LINKS*46)-1 downto 0);

    signal tx_usrclk                                            : std_logic_vector              ((NUM_LINKS-1) downto 0);
    signal rx_usrclk                                            : std_logic_vector              ((NUM_LINKS-1) downto 0);

    type wordReg_tx_A                                            is array (natural range <>) of word_mxnbit_A (0 to GBTTX_MGTREG_DEEP);
    signal wordReg_tx                               :  wordReg_tx_A          (1 to NUM_LINKS);

    signal MGT_USRWORD_s                                        : word_mxnbit_A(1 to NUM_LINKS);
    signal bitSlipCmd_to_bitSlipCtrller                    : std_logic_vector(1 to NUM_LINKS);
    signal ready_from_bitSlipCtrller                        : std_logic_vector(1 to NUM_LINKS);

	 signal rx_bitslipIsEven_s            : std_logic_vector(1 to NUM_LINKS);
	 signal rx_headerlocked_s             : std_logic_vector(1 to NUM_LINKS);
	 signal rx_rstonbitslip_s             : std_logic_vector(1 to NUM_LINKS);
signal tx_serial_clk : std_logic;
    --=====================================================================================--

--=================================================================================================--
begin                 --========####   Architecture Body   ####========--
--=================================================================================================--

-----------------debug-------------------- 
            TX_ANALOGRESET_DEBUG    <= txAnalogReset_from_gxRstCtrl; 
            TX_DIGITALRESET_DEBUG   <= txDigitalReset_from_gxRstCtrl; 
            TX_READY_DEBUG          <= txReady_from_gxRstCtrl; 
            RX_ANALOGRESET_DEBUG    <= rxAnalogreset_from_gxRstCtrl; 
            RX_DIGITALRESET_DEBUG   <= rxDigitalreset_from_gxRstCtrl; 
            RX_READY_DEBUG          <= rxReady_from_gxRstCtrl; 
            RX_CAL_BUSY             <= rxCalBusy_from_gxLatOpt;
            RX_IS_LOCKEDTODATA      <= rxIsLockedToData_from_gxLatOpt;
-----------------debug-------------------- 
 

   --==================================== User Logic =====================================--

   --=============--
   -- Assignments --
   --=============--
   commonAssign_gen: for i in 1 to NUM_LINKS generate


      MGT_TXREADY_o(i)          <= txReady_from_gxRstCtrl(i);
      MGT_RXREADY_o(i)          <= rxReady_from_gxRstCtrl(i) and done_from_rxBitSlipControl(i);

      MGT_RXUSRCLK_o(i)         <= rx_usrclk(i-1);
      MGT_TXUSRCLK_o(i)         <= tx_usrclk(i-1);

      wordReg_tx(i)(0) <= MGT_USRWORD_i(i);


        -- Timing issue: flip-flop stages (configurable)
        timmingIssueReg_gen: for j in 1 to GBTTX_MGTREG_DEEP generate

            fifoProc: process(txReady_from_gxRstCtrl(i), tx_usrclk(i-1))
            begin
                if txReady_from_gxRstCtrl(i) = '0' then
                    wordReg_tx(i)(j) <= (others => '0');

                elsif rising_edge(tx_usrclk(i-1)) then
                    wordReg_tx(i)(j) <= wordReg_tx(i)(j-1);

                end if;
            end process;

        end generate;

   end generate;

   MGT_USRWORD_o    <= MGT_USRWORD_s;

   --======================--
   -- GX reset controllers --
   --======================--
   gxRstCtrl_gen: for i in 1 to NUM_LINKS generate

      gxRstCtrl: entity work.alt_ax_mgt_resetctrl
         port map (
            CLK_I                                  => MGT_REFCLK_i,
            ---------------------------------------
            TX_RESET_I                             => MGT_TXRESET_i(i) or resetGxTx_from_rxBitSlipControl(i),
            RX_RESET_I                             => MGT_RXRESET_i(i) or resetGxRx_from_rxBitSlipControl(i),
            ---------------------------------------
            TX_ANALOGRESET_O                       => txAnalogReset_from_gxRstCtrl(i),
            TX_ANALOGRESET_STAT_I                  => tx_analogreset_stat_s(i),
            TX_DIGITALRESET_O                      => txDigitalReset_from_gxRstCtrl(i),
            TX_DIGITALRESET_STAT_I                 => tx_digitalreset_stat_s(i),
            TX_READY_O                             => txReady_from_gxRstCtrl(i),
            PLL_LOCKED_I                           => TXPLLLocked,
            TX_CAL_BUSY_I                          => txCalBusy_from_gxLatOpt(i),
            ---------------------------------------
            RX_ANALOGRESET_O                       => rxAnalogreset_from_gxRstCtrl(i),
            RX_ANALOGRESET_STAT_I                  => rx_analogreset_stat_s(i),
            RX_DIGITALRESET_O                      => rxDigitalreset_from_gxRstCtrl(i),
            RX_DIGITALRESET_STAT_I                 => rx_digitalreset_stat_s(i),
            RX_READY_O                             => rxReady_from_gxRstCtrl(i),
            RX_IS_LOCKEDTODATA_I                   => rxIsLockedToData_from_gxLatOpt(i),
            RX_CAL_BUSY_I                          => rxCalBusy_from_gxLatOpt(i)
         );

   end generate;

   --================================================--
   -- ATX PLL                                                      --
   --================================================--
    mgt_tx_pll: entity work.alt_mgt_txpll(stratix10)
        port map (
            RESET_I                             => MGT_TXRESET_i(1) or resetGxTx_from_rxBitSlipControl(1),

            MGT_REFCLK_I                        => MGT_REFCLK_i,
            TX_SERIAL_CLK                       =>tx_serial_clk,
            TX_BONDING_CLK_O                      => tx_bonding_clocks,

            LOCKED_O                            => TXPLLLocked,

            tx_pll_reconfig_write          => MGT_DEVSPEC_i.txpll_reconf_avmm_write,
            tx_pll_reconfig_read           => MGT_DEVSPEC_i.txpll_reconf_avmm_read,
            tx_pll_reconfig_address        => MGT_DEVSPEC_i.txpll_reconf_avmm_addr,
            tx_pll_reconfig_writedata      => MGT_DEVSPEC_i.txpll_reconf_avmm_writedata,
            tx_pll_reconfig_readdata       => MGT_DEVSPEC_o.txpll_reconf_avmm_readdata,
            tx_pll_reconfig_waitrequest    => MGT_DEVSPEC_o.txpll_reconf_avmm_waitrequest,
            tx_pll_reconfig_clk            => MGT_DEVSPEC_i.txpll_reconf_clk,
            tx_pll_reconfig_reset          => MGT_DEVSPEC_i.txpll_reconf_reset

        );

   --============================--
   -- Multi-Gigabit Transceivers --
   --============================--
   gxLatOpt_x1_gen: if NUM_LINKS = 1 generate

      gxLatOpt_x1: entity gx_x1.gx_x1
            port map(

                -- Reconfigurator avalon MM bus
                reconfig_write(0)                         => MGT_DEVSPEC_i.reconf_avmm_write,                 --           reconfig_avmm.write
                reconfig_read(0)                          => MGT_DEVSPEC_i.reconf_avmm_read,                     --                        .read
                reconfig_address                        => MGT_DEVSPEC_i.reconf_avmm_addr(10 downto 0),     --                        .address
                reconfig_writedata                      => MGT_DEVSPEC_i.reconf_avmm_writedata,            --                        .writedata
                reconfig_readdata                       => MGT_DEVSPEC_o.reconf_avmm_readdata,             --                        .readdata
                reconfig_waitrequest(0)                   => MGT_DEVSPEC_o.reconf_avmm_waitrequest,         --                        .waitrequest
                reconfig_clk(0)                           => MGT_DEVSPEC_i.reconf_clk,                             --            reconfig_clk.clk
                reconfig_reset(0)                         => MGT_DEVSPEC_i.reconf_reset,                         --          reconfig_reset.reset

                -- Reset
                rx_analogreset(0)                         => rxAnalogReset_from_gxRstCtrl(1),                    --          rx_analogreset.rx_analogreset
                rx_digitalreset(0)                        => rxDigitalReset_from_gxRstCtrl(1),                --         rx_digitalreset.rx_digitalreset


                rx_cal_busy(0)                          => rxCalBusy_from_gxLatOpt(1),                        --             rx_cal_busy.rx_cal_busy
                tx_cal_busy(0)                                => txCalBusy_from_gxLatOpt(1),                        --             tx_cal_busy.tx_cal_busy

                rx_is_lockedtodata(0)                     => rxIsLockedToData_from_gxLatOpt(1),                 --      rx_is_lockedtodata.rx_is_lockedtodata

                tx_analogreset(0)                         => txAnalogReset_from_gxRstCtrl(1),                    --          tx_analogreset.tx_analogreset
                tx_digitalreset(0)                        => txDigitalReset_from_gxRstCtrl(1),                --         tx_digitalreset.tx_digitalreset

                tx_analogreset_stat(0)                       => tx_analogreset_stat_s(1),  -- tx_analogreset_stat
                
                rx_analogreset_stat(0)                       => rx_analogreset_stat_s(1),  -- rx_analogreset_stat
                
                tx_digitalreset_stat(0)                      => tx_digitalreset_stat_s(1), -- tx_digitalreset_stat

                rx_digitalreset_stat(0)                      => rx_digitalreset_stat_s(1), -- rx_digitalreset_stat


                -- RX parallel data
                rx_parallel_data(19  downto 0)         => MGT_USRWORD_s(1),                                    --        rx_parallel_data.rx_parallel_data

                -- TX parallel data
                tx_parallel_data(19  downto 0)         => wordReg_tx(1)(GBTTX_MGTREG_DEEP),                 --        tx_parallel_data.tx_parallel_data

                -- Configuration
                rx_seriallpbken(0)                       => MGT_DEVSPEC_i.loopBack(1),                         --         rx_seriallpbken.rx_seriallpbken
                rx_polinv(0)                             => MGT_DEVSPEC_i.rx_polarity(1),                        --               tx_polinv.tx_polinv
                tx_polinv(0)                             => MGT_DEVSPEC_i.tx_polarity(1),                        --               tx_polinv.tx_polinv
                rx_pma_clkslip(0)                        => rxBitSlip_to_gxLatOpt(1),                             --          rx_pma_clkslip.rx_pma_clkslip

                -- Clocks
                rx_cdr_refclk0                               => MGT_REFCLK_i,                                         --          rx_cdr_refclk0.clk

                rx_clkout                                   => rx_usrclk,                                           --               rx_clkout.clk
                rx_coreclkin                                => rx_usrclk,                                                --            rx_coreclkin.clk

                tx_clkout                                   => tx_usrclk,                                           --               tx_clkout.clk
                tx_coreclkin                                => tx_usrclk,                                                 --            tx_coreclkin.clk
                  
                tx_serial_clk0(0)                            =>tx_serial_clk,
--                tx_bonding_clocks(5 downto 0)          => tx_bonding_clocks,                                    --       tx_bonding_clocks.clk

                -- Serial link
                tx_serial_data(0)                        => MGT_DEVSPEC_o.txSerialData(1),                 --          tx_serial_data.tx_serial_data
                rx_serial_data(0)                           => MGT_DEVSPEC_i.rxSerialData(1),                        --          rx_serial_data.rx_serial_data

                -- Unused
                unused_rx_parallel_data                   => open,                                                -- unused_rx_parallel_data.unused_rx_parallel_data
                unused_tx_parallel_data                   => open                                                      -- unused_tx_parallel_data.unused_tx_parallel_data
            );
   end generate;
--
   gxLatOpt_x2_gen: if NUM_LINKS = 2 generate

      gxLatOpt_x2: entity gx_x2.gx_x2
            port map(

                -- Reconfigurator avalon MM bus
                reconfig_write(0)                         => MGT_DEVSPEC_i.reconf_avmm_write,                 --           reconfig_avmm.write
                reconfig_read(0)                          => MGT_DEVSPEC_i.reconf_avmm_read,                     --                        .read
                reconfig_address                        => MGT_DEVSPEC_i.reconf_avmm_addr(11 downto 0), --                        .address
                reconfig_writedata                      => MGT_DEVSPEC_i.reconf_avmm_writedata,            --                        .writedata
                reconfig_readdata                       => MGT_DEVSPEC_o.reconf_avmm_readdata,             --                        .readdata
                reconfig_waitrequest(0)                   => MGT_DEVSPEC_o.reconf_avmm_waitrequest,         --                        .waitrequest
                reconfig_clk(0)                           => MGT_DEVSPEC_i.reconf_clk,                             --            reconfig_clk.clk
                reconfig_reset(0)                         => MGT_DEVSPEC_i.reconf_reset,                         --          reconfig_reset.reset

                -- Reset
                rx_analogreset(0)                         => rxAnalogReset_from_gxRstCtrl(1),                    --          rx_analogreset.rx_analogreset
                rx_analogreset(1)                         => rxAnalogReset_from_gxRstCtrl(2),                    --          rx_analogreset.rx_analogreset

                rx_digitalreset(0)                        => rxDigitalReset_from_gxRstCtrl(1),                --         rx_digitalreset.rx_digitalreset
                rx_digitalreset(1)                        => rxDigitalReset_from_gxRstCtrl(2),                --         rx_digitalreset.rx_digitalreset

                rx_cal_busy(0)                          => rxCalBusy_from_gxLatOpt(1),                        --             rx_cal_busy.rx_cal_busy
                rx_cal_busy(1)                          => rxCalBusy_from_gxLatOpt(2),                        --             rx_cal_busy.rx_cal_busy

                rx_is_lockedtodata(0)                     => rxIsLockedToData_from_gxLatOpt(1),                 --      rx_is_lockedtodata.rx_is_lockedtodata
                rx_is_lockedtodata(1)                     => rxIsLockedToData_from_gxLatOpt(2),                 --      rx_is_lockedtodata.rx_is_lockedtodata

                tx_analogreset(0)                         => txAnalogReset_from_gxRstCtrl(1),                    --          tx_analogreset.tx_analogreset
                tx_analogreset(1)                         => txAnalogReset_from_gxRstCtrl(2),                    --          tx_analogreset.tx_analogreset

                tx_digitalreset(0)                        => txDigitalReset_from_gxRstCtrl(1),                --         tx_digitalreset.tx_digitalreset
                tx_digitalreset(1)                        => txDigitalReset_from_gxRstCtrl(2),                --         tx_digitalreset.tx_digitalreset

                tx_cal_busy(0)                                => txCalBusy_from_gxLatOpt(1),                        --             tx_cal_busy.tx_cal_busy
                tx_cal_busy(1)                                => txCalBusy_from_gxLatOpt(2),                        --             tx_cal_busy.tx_cal_busy
                
                tx_analogreset_stat(0)                       => tx_analogreset_stat_s(1),  -- tx_analogreset_stat
                tx_analogreset_stat(1)                       => tx_analogreset_stat_s(2),  -- tx_analogreset_stat
                
                rx_analogreset_stat(0)                       => rx_analogreset_stat_s(1),  -- rx_analogreset_stat
                rx_analogreset_stat(1)                      => rx_analogreset_stat_s(2),  -- rx_analogreset_stat
                
                tx_digitalreset_stat(0)                      => tx_digitalreset_stat_s(1), -- tx_digitalreset_stat
                tx_digitalreset_stat(1)                      => tx_digitalreset_stat_s(2), -- tx_digitalreset_stat

                rx_digitalreset_stat(0)                      => rx_digitalreset_stat_s(1), -- rx_digitalreset_stat
                rx_digitalreset_stat(1)                      => rx_digitalreset_stat_s(2), -- rx_digitalreset_stat

                -- RX parallel data
                rx_parallel_data(19  downto 0)     => MGT_USRWORD_s(1),                                    --        rx_parallel_data.rx_parallel_data
                rx_parallel_data(39  downto 20)    => MGT_USRWORD_s(2),                                    --        rx_parallel_data.rx_parallel_data

                -- TX parallel data
                tx_parallel_data(19  downto 0)     => wordReg_tx(1)(GBTTX_MGTREG_DEEP),                --        tx_parallel_data.tx_parallel_data
                tx_parallel_data(39  downto 20)    => wordReg_tx(2)(GBTTX_MGTREG_DEEP),                --        tx_parallel_data.tx_parallel_data

                -- Configuration
                rx_seriallpbken(0)                   => MGT_DEVSPEC_i.loopBack(1),                             --         rx_seriallpbken.rx_seriallpbken
                rx_seriallpbken(1)                   => MGT_DEVSPEC_i.loopBack(2),                             --         rx_seriallpbken.rx_seriallpbken

                rx_polinv(0)                         => MGT_DEVSPEC_i.rx_polarity(1),                        --               tx_polinv.tx_polinv
                rx_polinv(1)                         => MGT_DEVSPEC_i.rx_polarity(2),                        --               tx_polinv.tx_polinv

                tx_polinv(0)                         => MGT_DEVSPEC_i.tx_polarity(1),                        --               tx_polinv.tx_polinv
                tx_polinv(1)                         => MGT_DEVSPEC_i.tx_polarity(2),                        --               tx_polinv.tx_polinv

                rx_pma_clkslip(0)                    => rxBitSlip_to_gxLatOpt(1),                             --          rx_pma_clkslip.rx_pma_clkslip
                rx_pma_clkslip(1)                    => rxBitSlip_to_gxLatOpt(2),                             --          rx_pma_clkslip.rx_pma_clkslip

                -- Clocks
                rx_cdr_refclk0                           => MGT_REFCLK_i,                                         --          rx_cdr_refclk0.clk

                rx_clkout                               => rx_usrclk,                                               --               rx_clkout.clk
                rx_coreclkin                            => rx_usrclk,                                                --            rx_coreclkin.clk

                tx_clkout                               => tx_usrclk,                                               --               tx_clkout.clk
                tx_coreclkin                            => tx_usrclk,                                                 --            tx_coreclkin.clk
--no bonding because is not need to minimaze skew between channels
                tx_bonding_clocks(5 downto 0)      => tx_bonding_clocks,                                        --       tx_bonding_clocks.clk
                tx_bonding_clocks(11 downto 6)     => tx_bonding_clocks,                                     --       tx_bonding_clocks.clk

                -- Serial link
                tx_serial_data(0)                    => MGT_DEVSPEC_o.txSerialData(1),                 --          tx_serial_data.tx_serial_data
                tx_serial_data(1)                    => MGT_DEVSPEC_o.txSerialData(2),                 --          tx_serial_data.tx_serial_data

                rx_serial_data(0)                       => MGT_DEVSPEC_i.rxSerialData(1),                        --          rx_serial_data.rx_serial_data
                rx_serial_data(1)                       => MGT_DEVSPEC_i.rxSerialData(2),                        --          rx_serial_data.rx_serial_data

                -- Unused
                unused_rx_parallel_data               => open,                                                   -- unused_rx_parallel_data.unused_rx_parallel_data
                unused_tx_parallel_data               => open                                                          -- unused_tx_parallel_data.unused_tx_parallel_data
            );
   end generate;
    --====================--
   -- RX phase alignment --
   --====================--
   rxPhaseAlign_numLinks_gen: for i in 1 to NUM_LINKS generate

			-- Reset on bitslip control module:
         -----------------------------------
			bitslipResetFSM_proc: PROCESS(MGT_REFCLK_i, MGT_RXRESET_i(i))
			  variable timer :integer range 0 to GBTRX_BITSLIP_MGT_RX_RESET_DELAY;
           variable rstcnt: unsigned(7 downto 0);
			begin

				if MGT_RXRESET_i(i) = '1' then
					 resetGxRx_from_rxBitSlipControl(i) <= '0';
					 resetGxTx_from_rxBitSlipControl(i) <= '0';
					 timer  := 0;
					 rstcnt := (others => '0');
					 rstBitSlip_FSM(i) <= idle;

				elsif rising_edge(MGT_REFCLK_i) then

					 case rstBitSlip_FSM(i) is
						when idle      => resetGxRx_from_rxBitSlipControl(i)     <= '0';
												resetGxTx_from_rxBitSlipControl(i)     <= '0';

												if rx_rstonbitslip_s(i) = '1' then

													resetGxRx_from_rxBitSlipControl(i) <= '1';
													resetGxTx_from_rxBitSlipControl(i) <= '1';
													rstBitSlip_FSM(i) <= reset_tx;
													timer := 0;

													rstcnt := rstcnt+1;

												end if;

						when reset_tx  => if timer = GBTRX_BITSLIP_MGT_RX_RESET_DELAY-1 then
													 resetGxTx_from_rxBitSlipControl(i)     <= '0';
													 rstBitSlip_FSM(i)                       <= reset_rx;
													 timer                                   := 0;
												else
													 timer := timer + 1;
												end if;

						when reset_rx  => if timer = GBTRX_BITSLIP_MGT_RX_RESET_DELAY-1 then
													  resetGxRx_from_rxBitSlipControl(i)     <= '0';
													  rstBitSlip_FSM(i)                       <= idle;
													  timer                                   := 0;
												 else
													  timer := timer + 1;
												 end if;

					 end case;

				    MGT_RSTCNT_o(i)   <= std_logic_vector(rstcnt);
				end if;

			end process;

         rxBitSlipControl: entity work.mgt_bitslipctrl
            port map (
                    RX_RESET_I          => not(rxReady_from_gxRstCtrl(i)),
                    RX_WORDCLK_I        => rx_usrclk(i-1),
						  MGT_CLK_I           => MGT_REFCLK_i,

                    RX_BITSLIPCMD_i     => bitSlipCmd_to_bitSlipCtrller(i),
                    RX_BITSLIPCMD_o     => rxBitSlip_to_gxLatOpt(i),

                    RX_HEADERLOCKED_i   => rx_headerlocked_s(i),
                    RX_BITSLIPISEVEN_i  => rx_bitslipIsEven_s(i),
                    RX_RSTONBITSLIP_o   => rx_rstonbitslip_s(i),
                    RX_ENRST_i          => MGT_AUTORSTEn_i(i),
                    RX_RSTONEVEN_i      => MGT_AUTORSTONEVEN_i(i),

						  DONE_o              => done_from_rxBitSlipControl(i),
						  READY_o             => ready_from_bitSlipCtrller(i)
            );

            patternSearch: entity work.mgt_framealigner_pattsearch
                port map (
                    RX_RESET_I                          => not(rxReady_from_gxRstCtrl(i)),
                    RX_WORDCLK_I                        => rx_usrclk(i-1),

                    RX_BITSLIP_CMD_O                    => bitSlipCmd_to_bitSlipCtrller(i),
                    MGT_BITSLIPDONE_i                   => ready_from_bitSlipCtrller(i),

                    RX_HEADER_LOCKED_O                  => rx_headerlocked_s(i),
                    RX_HEADER_FLAG_O                    => RX_HEADERFLAG_o(i),
                    RX_BITSLIPISEVEN_o                  => rx_bitslipIsEven_s(i),

                    RX_WORD_I                           => MGT_USRWORD_s(i)
                );

				RX_HEADERLOCKED_o(i) <= rx_headerlocked_s(i);

   end generate;

   --=====================================================================================--
end structural;
--=================================================================================================--
--#################################################################################################--
--=================================================================================================--
