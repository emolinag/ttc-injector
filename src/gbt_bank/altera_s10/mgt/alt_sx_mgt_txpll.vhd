-------------------------------------------------------
--! @file
--! @author Emmanuel MOLINA GONZALEZ <emmanuel-josue.molina-gonzalez@cern.ch> 
--! @version 6.0??
--! @brief GBT-FPGA IP - Transceiver Tx PLL for stratix 10. architecture
-------------------------------------------------------

--! @brief ALT_ax_mgt_txpll - Transceiver Tx PLL
--! @details 
--! The ALT_ax_mgt_txpll provides the serial clock to the transmitter
--! @brief ALT_ax_mgt_txpll architecture - Transceiver Tx PLL
--! @details The ALT_ax_mgt_txpll implement the transmitter pll (ATXPLL or FPLL depending on user configuration)
architecture stratix10 of alt_mgt_txpll is 

  component mgt_fpll is
    port (
      pll_refclk0           : in  std_logic                     := 'X';             -- clk
      tx_serial_clk         : out std_logic;                                        -- clk
      pll_locked            : out std_logic;                                        -- pll_locked
      reconfig_clk0         : in  std_logic                     := 'X';             -- clk
      reconfig_reset0       : in  std_logic                     := 'X';             -- reset
      reconfig_write0       : in  std_logic                     := 'X';             -- write
      reconfig_read0        : in  std_logic                     := 'X';             -- read
      reconfig_address0     : in  std_logic_vector(10 downto 0) := (others => 'X'); -- address
      reconfig_writedata0   : in  std_logic_vector(31 downto 0) := (others => 'X'); -- writedata
      reconfig_readdata0    : out std_logic_vector(31 downto 0);                    -- readdata
      reconfig_waitrequest0 : out std_logic;                                        -- waitrequest
      pll_cal_busy          : out std_logic;                                        -- pll_cal_busy
      tx_bonding_clocks     : out std_logic_vector(5 downto 0)                      -- clk
    );
  end component mgt_fpll;


   --=====================================================================================--   
   
--=================================================================================================--
begin                 --========####   Architecture Body   ####========-- 
--=================================================================================================--
   
   --==================================== User Logic =====================================-- 
   
  --=============--
  -- GTX PLL Rst --
  --=============--
  fpll_gen: if XCVR_TX_PLL = FPLL generate
    transceiver_mgtpll: mgt_fpll
      port map(
        pll_refclk0           => MGT_REFCLK_I,                    -- clk
        tx_serial_clk         => open,                            -- clk
        pll_locked            => LOCKED_O,                        -- pll_locked
        reconfig_clk0         => tx_pll_reconfig_clk,             -- clk
        reconfig_reset0       => tx_pll_reconfig_reset,           -- reset
        reconfig_write0       => tx_pll_reconfig_write,           -- write
        reconfig_read0        => tx_pll_reconfig_read,            -- read
        reconfig_address0     => tx_pll_reconfig_address,         -- address
        reconfig_writedata0   => tx_pll_reconfig_writedata,       -- writedata
        reconfig_readdata0    => tx_pll_reconfig_readdata,        -- readdata
        reconfig_waitrequest0 => tx_pll_reconfig_waitrequest,     -- waitrequest
        pll_cal_busy          => open,                             -- pll_cal_busy
        tx_bonding_clocks     => TX_BONDING_CLK_O                 -- clk
      );
  end generate fpll_gen;

  ATXpll_gen: if XCVR_TX_PLL = ATXPLL generate
    transceiver_atxpll: entity mgt_atxpll.mgt_atxpll
       port map(
         pll_refclk0           => MGT_REFCLK_I,                -- clk
         tx_serial_clk         => tx_serial_clk,                        -- clk
         pll_locked            => LOCKED_O,                    -- pll_locked
         reconfig_clk0         => tx_pll_reconfig_clk,         -- clk
         reconfig_reset0       => tx_pll_reconfig_reset,       -- reset
         reconfig_write0       => tx_pll_reconfig_write,       -- write
         reconfig_read0        => tx_pll_reconfig_read,        -- read
         reconfig_address0     => tx_pll_reconfig_address,     -- address
         reconfig_writedata0   => tx_pll_reconfig_writedata,   -- writedata
         reconfig_readdata0    => tx_pll_reconfig_readdata,    -- readdata
         reconfig_waitrequest0 => tx_pll_reconfig_waitrequest, -- waitrequest
         pll_cal_busy          => open,                         -- pll_cal_busy
         tx_bonding_clocks     => TX_BONDING_CLK_O             -- clk
       );
  end generate ATXpll_gen;     
   --=====================================================================================--   
end stratix10;
--=================================================================================================--
--#################################################################################################--
--=================================================================================================--
