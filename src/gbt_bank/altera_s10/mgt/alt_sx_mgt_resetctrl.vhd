-------------------------------------------------------
--! @file
--! @author Julian Mendez <julian.mendez@cern.ch> (CERN - EP-ESE-BE)
--! @version 6.0
--! @brief GBT-FPGA IP - Transceiver reset
-------------------------------------------------------

--! IEEE VHDL standard library:
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

--! Altera devices library:
library altera;
library altera_mf;
library lpm;
use altera.altera_primitives_components.all;
use altera_mf.altera_mf_components.all;
use lpm.lpm_components.all;

--! Libraries for direct instantiation:
library gx_reset_tx;
library gx_reset_rx;

--! @brief ALT_ax_mgt_resetctrl - Transceiver reset
--! @details
--! The ALT_ax_mgt_resetctrl manages the Tx and Rx resets of the transceiver
entity alt_ax_mgt_resetctrl is
   generic (
     constant STRATIX10   : integer:=1;
     constant ARRIA10     : integer:=0;
     constant FPGA_FAMILY : integer:=1
   );
   port (

      --=======--
      -- Clock --
      --=======--

      CLK_I                                     : in  std_logic;

      --===============--
      -- Resets scheme --
      --===============--

      -- TX reset control:
      --------------------

      TX_RESET_I                                : in  std_logic;
      ------------------------------------------
      TX_ANALOGRESET_O                          : out std_logic;
      TX_ANALOGRESET_STAT_I                     : in  std_logic;
      TX_DIGITALRESET_O                         : out std_logic;
      TX_DIGITALRESET_STAT_I                    : in std_logic;
      TX_READY_O                                : out std_logic;
      PLL_LOCKED_I                              : in  std_logic;
      TX_CAL_BUSY_I                             : in  std_logic;

      -- RX reset control:
      --------------------

      RX_RESET_I                                : in  std_logic;
      ------------------------------------------
      RX_ANALOGRESET_O                          : out std_logic;
      RX_ANALOGRESET_STAT_I                     : in std_logic;
      RX_DIGITALRESET_O                         : out std_logic;
      RX_DIGITALRESET_STAT_I                    : in std_logic;
      RX_READY_O                                : out std_logic;
      RX_IS_LOCKEDTODATA_I                      : in  std_logic;
      RX_CAL_BUSY_I                             : in  std_logic

   );
end alt_ax_mgt_resetctrl;

--! @brief ALT_ax_mgt_resetctrl - Transceiver reset
--! @details The ALT_ax_mgt_resetctrl implements the transceiver's reset controller IP (device specific)
architecture structural of alt_ax_mgt_resetctrl is

--=================================================================================================--
begin                 --========####   Architecture Body   ####========--
--=================================================================================================--

   --==================================== User Logic =====================================--

   --======================--
   -- GX reset controllers --
   --======================--

   arria10Reset: if FPGA_FAMILY=ARRIA10 generate
   -- TX reset:
   ------------
     gxResetTx:entity gx_reset_tx.gx_reset_tx
        port map (
           CLOCK                                  => CLK_I,
           RESET                                  => TX_RESET_I,
           TX_ANALOGRESET(0)                      => TX_ANALOGRESET_O,
           TX_DIGITALRESET(0)                     => TX_DIGITALRESET_O,
           TX_READY(0)                            => TX_READY_O,
           PLL_LOCKED(0)                          => PLL_LOCKED_I,
           PLL_SELECT(0)                          => '0',
           TX_CAL_BUSY(0)                         => TX_CAL_BUSY_I
        );

   -- RX reset:
   ------------
     gxResetRx: entity gx_reset_rx.gx_reset_rx
        port map (
           CLOCK                                  => CLK_I,
           RESET                                  => RX_RESET_I,
           RX_ANALOGRESET(0)                      => RX_ANALOGRESET_O,
           RX_DIGITALRESET(0)                     => RX_DIGITALRESET_O,
           RX_READY(0)                            => RX_READY_O,
           RX_IS_LOCKEDTODATA(0)                  => RX_IS_LOCKEDTODATA_I,
           RX_CAL_BUSY(0)                         => RX_CAL_BUSY_I
        );
   end generate arria10Reset;

   stratix10Reset: if FPGA_FAMILY=STRATIX10 generate

     gxResetTx:entity gx_reset_tx.gx_reset_tx
        port map (
           CLOCK                                  => CLK_I,
           RESET                                  => TX_RESET_I,
           TX_ANALOGRESET(0)                      => TX_ANALOGRESET_O,
           TX_DIGITALRESET(0)                     => TX_DIGITALRESET_O,
           TX_READY(0)                            => TX_READY_O,
           PLL_LOCKED(0)                          => PLL_LOCKED_I,
           PLL_SELECT(0)                          => '0',
           TX_CAL_BUSY(0)                         => TX_CAL_BUSY_I,
           TX_ANALOGRESET_STAT(0)                 => TX_ANALOGRESET_STAT_I,-- in std_logic_vector(0 downto 0)
           TX_DIGITALRESET_STAT(0)                => TX_DIGITALRESET_STAT_I-- in std_logic_vector(0 downto 0)
           );
     -- RX reset:
     ------------
     gxResetRx: entity gx_reset_rx.gx_reset_rx
               port map (
                         CLOCK                                  => CLK_I,
                         RESET                                  => RX_RESET_I,
                         RX_ANALOGRESET(0)                      => RX_ANALOGRESET_O,
                         RX_DIGITALRESET(0)                     => RX_DIGITALRESET_O,
                         RX_READY(0)                            => RX_READY_O,
                         RX_IS_LOCKEDTODATA(0)                  => RX_IS_LOCKEDTODATA_I,
                         RX_CAL_BUSY(0)                         => RX_CAL_BUSY_I,
                         RX_ANALOGRESET_STAT(0)                 => RX_ANALOGRESET_STAT_I,-- in std_logic_vector(0 downto 0)
           RX_DIGITALRESET_STAT(0)                => RX_DIGITALRESET_STAT_I-- in std_logic_vector(0 downto 0)

        );
   end generate stratix10Reset;

   --=====================================================================================--
end structural;
--=================================================================================================--
--#################################################################################################--
--=================================================================================================--
