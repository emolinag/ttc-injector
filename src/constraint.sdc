**************************************************************
# Time Information
#**************************************************************
set_time_format -unit ns -decimal_places 3

#**************************************************************
# Create Clock
#**************************************************************
create_clock -period 25 [get_ports {refclk_i}]
create_clock -period 4.166 [get_ports {mgtclk_i}]

create_clock -name altera_reserved_tck [get_ports {altera_reserved_tck}] -period "24 MHz"
#**************************************************************
# Create Generated Clock
#**************************************************************

#create_generated_clock -name {*gbtExmplDsgn_inst*gatedclock_gen*tmp} -source {*gbtExmplDsgn_inst*gatedclock_gen*RX_WORDCLK} -divide_by 6
#**************************************************************
# Set Clock Latency
#**************************************************************

#**************************************************************
# Set Clock Uncertainty
#**************************************************************

#**************************************************************
# Set Input Delay
#**************************************************************
set_input_delay -clock altera_reserved_tck -clock_fall 3 [get_ports altera_reserved_tdi]
set_input_delay -clock altera_reserved_tck -clock_fall 3 [get_ports altera_reserved_tms]
set_output_delay -clock altera_reserved_tck -clock_fall 3 [get_ports altera_reserved_tdo]


#**************************************************************
# Set False Path
#**************************************************************
#set_false_path -from {*gbt_inst*r_reset}
set_false_path  -from *inst_issp*
set_false_path  -to *inst_issp*

#**************************************************************
# Set Multicycle Path
#**************************************************************

#**************************************************************
# Set Maximum Delay
#**************************************************************

#**************************************************************
# Set Minimum Delay
#**************************************************************

#**************************************************************
# Set Input Transition
#**************************************************************

#**************************************************************
# Set Max Skew
#**************************************************************
set_max_skew -from *gx_reset_tx:gxResetTx*tx_digitalreset*r_reset -to *pld_pcs_interface* 2.08

