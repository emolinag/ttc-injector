library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity measure_freq is
  port(
     clk_ref_i   : in std_logic;
     clk_measure_i: in std_logic;
     nrst_i      : in std_logic;
     valid_freq_o: out std_logic;
     freq_o      : out std_logic_vector(7 downto 0)     
  );
end measure_freq;

architecture behavioral of measure_freq is

   signal nrst_measure_freq : std_logic:='0';
   signal nrst_temp         : std_logic;
   signal r_done_ref        : std_logic;
   signal r_done_ref_temp   : std_logic_vector(3 downto 0);
   signal r_done            : std_logic;
   signal r_done_q          : std_logic;
   signal r_done_temp       : std_logic_vector(1 downto 0);
   signal r_valid           : std_logic;
   signal r_value           : std_logic_vector(7 downto 0);
   signal r_valid_temp      : std_logic_vector(3 downto 0);
   signal r_valid_ref       : std_logic;

begin

-- Domain 100Mhz
proc_base_freq: process(clk_ref_i)
     variable counter : integer range 0 to 100;
  begin
     if rising_edge(clk_ref_i) then
       if nrst_i = '0' then
         counter    := 0;
         r_done_ref <= '0';
       else
         if counter = 100 then
           r_done_ref <= '1';
           counter    := 0;
         else
           r_done_ref <= '0';
           counter    := counter + 1;
         end if;
       end if;
     end if;
end process;

proc_done_tmp: process(clk_ref_i)
  begin
    if rising_edge(clk_ref_i) then
      if nrst_i = '0' then
         r_done_ref_temp <= (others => '0');
      else
         r_done_ref_temp <= r_done_ref_temp(2 downto 0) & '0';
         if r_done_ref = '1' then
            r_done_ref_temp <= (others =>'1');
         end if;
      end if;
    end if;
end process;

freq_o       <= r_value;
valid_freq_o <= r_valid_ref;

--************************************
-- CDC 
--************************************

--CDC of reset
proc_cdc_nrst: process(clk_measure_i)
  begin
    if rising_edge(clk_measure_i) then
       nrst_temp         <= nrst_i;
       nrst_measure_freq <= nrst_temp;
    end if;
end process;

--CDC of  r_valid
proc_cdc_value: process(clk_ref_i)
  begin
    if rising_edge(clk_ref_i) then
      r_valid_temp <= r_valid_temp(2 downto 0) & r_valid;
      r_valid_ref  <= r_valid_temp(3);
    end if;
end process;

--CDC of r_done signal
proc_cdc_done: process(clk_measure_i)
  begin
    if rising_edge(clk_measure_i) then
      r_done_temp <= r_done_temp(0) & r_done_ref_temp(3);
      r_done      <= r_done_temp(1);
    end if;
end process;

--************************************


proc_rising_detector: process(clk_measure_i)
  begin
     if rising_edge(clk_measure_i) then
       if nrst_measure_freq = '0' then
         r_done_q <='0';
       else
         r_done_q <= r_done;
       end if;
     end if;
end process;


-- Another domain in Mhz
proce_measure_freq: process(clk_measure_i)
     variable counter : integer range 0 to 250;
  begin
     if rising_edge(clk_measure_i) then
       if nrst_measure_freq = '0' then
         counter := 0;
         r_valid <='0';
         r_value <=(others =>'0');
       else
         if (r_done  and not r_done_q ) then
           r_value <= std_logic_vector(to_unsigned(counter,r_value'length));
           r_valid <='1';
           counter := 0;
         else
           r_valid <='0';
           counter := counter + 1;
         end if;
       end if;
     end if;
end process;

end behavioral;
