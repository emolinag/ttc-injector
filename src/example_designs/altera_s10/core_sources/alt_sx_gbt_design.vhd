-- IEEE VHDL standard library:
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-- Altera devices library:
library altera;
library altera_mf;
library lpm;
use altera.altera_primitives_components.all;
use altera_mf.altera_mf_components.all;
use lpm.lpm_components.all;

-- Custom libraries and packages:
use work.gbt_bank_package.all;
use work.vendor_specific_gbt_bank_package.all;
use work.gbt_exampledesign_package.all;

entity alt_sx_gbt_design is
    generic (
        NUM_LINKS             : integer              := 2;
        TX_OPTIMIZATION       : integer range 0 to 1 := STANDARD;
        RX_OPTIMIZATION       : integer range 0 to 1 := STANDARD;
        TX_ENCODING           : integer range 0 to 2 := GBT_FRAME;
        RX_ENCODING           : integer range 0 to 2 := GBT_FRAME;
        DATA_GENERATOR_ENABLE : integer range 0 to 1 := 1;
        DATA_CHECKER_ENABLE   : integer range 0 to 1 := 1;
        MATCH_FLAG_ENABLE     : integer range 0 to 1 := 1;
        CLOCKING_SCHEME       : integer range 0 to 1 := BC_CLOCK
    );
    port (
        MGT_CLK_i                   : in  std_logic;  --is a port
        GBT_ISDATAFLAG_i            : in  std_logic_vector(1 to NUM_LINKS);  --??
        GBT_ISDATAFLAG_o            : out std_logic_vector(1 to NUM_LINKS);
        GBT_ERRORDETECTED_o         : out std_logic_vector(1 to NUM_LINKS);
        GBT_ERRORFLAG_o             : out gbt_reg84_A(1 to NUM_LINKS);
        GBT_TXDATA_i                : in  gbt_reg84_A(1 to NUM_LINKS);
        GBT_RXDATA_o                : out gbt_reg84_A(1 to NUM_LINKS);
        WB_TXDATA_i                 : in  gbt_reg32_A (1 to NUM_LINKS);
        WB_RXDATA_o                 : out gbt_reg32_A (1 to NUM_LINKS);
        FRAMECLK_40MHZ              : in std_logic;  --from outside
        GBT_GENERAL_RESET_I         : in  std_logic;
        GBTBANK_RX_FRAMECLK_READY_O : out std_logic_vector(1 to NUM_LINKS);
        GBT_RXFRAMECLK_O            : out std_logic_vector(1 to NUM_LINKS);
        GBT_TXFRAMECLK_i            : in std_logic_vector(1 to NUM_LINKS);
        MGT_TXWORDCLK_O             : out std_logic_vector(1 to NUM_LINKS);
        MGT_RXWORDCLK_O             : out std_logic_vector(1 to NUM_LINKS);
        MGT_HEADERFLAG_O            : out std_logic_vector(1 to NUM_LINKS);
        GBT_LINK_TX_READY_O         : out std_logic_vector(1 to NUM_LINKS);
        GBT_LINK_RX_READY_O         : out std_logic_vector(1 to NUM_LINKS);
        GBT_GBTRX_READY_O           : out std_logic_vector(1 to NUM_LINKS);
        GBT_LINK_READY_O            : out std_logic_vector(1 to NUM_LINKS);
        GBT_GBTTX_READY_O           : out std_logic_vector(1 to NUM_LINKS);
        GBT_MGT_RX                  : in  std_logic_vector(1 to NUM_LINKS);
        GBT_MGT_TX                  : out std_logic_vector(1 to NUM_LINKS);
        GBT_RXCLKEN_o               : out std_logic_vector(1 to NUM_LINKS);
        GBT_RXREADY_o               : out std_logic_vector(1 to NUM_LINKS);
        GBT_RXCLKENLOGIC_O          : out std_logic_vector(1 to NUM_LINKS);
        GBT_TXCLKEn_i               : in std_logic_vector(1 to NUM_LINKS);
        GBT_BANK_LOOPBACK           : in  std_logic;
--------------debug--------------------
        MGT_TXRESET_DEBUG           : out std_logic_vector(1 to NUM_LINKS);
        MGT_RXRESET_DEBUG           : out std_logic_vector(1 to NUM_LINKS);
        GBT_TXRESET_DEBUG           : out std_logic_vector(1 to NUM_LINKS);
        GBT_RXRESET_DEBUG           : out std_logic_vector(1 to NUM_LINKS);
        TX_ANALOGRESET_DEBUG        : out std_logic_vector(1 to NUM_LINKS);
        TX_DIGITALRESET_DEBUG       : out std_logic_vector(1 to NUM_LINKS);
        TX_READY_DEBUG              : out std_logic_vector(1 to NUM_LINKS);
        RX_ANALOGRESET_DEBUG        : out std_logic_vector(1 to NUM_LINKS);
        RX_DIGITALRESET_DEBUG       : out std_logic_vector(1 to NUM_LINKS);
        RX_READY_DEBUG              : out std_logic_vector(1 to NUM_LINKS);
        RX_CAL_BUSY                 : out std_logic_vector(1 to NUM_LINKS);
        RX_IS_LOCKEDTODATA          : out std_logic_vector(1 to NUM_LINKS)
        --TX_RESET_ISSP_I             : in std_logic_vector(1 to NUM_LINKS)
  );
end alt_sx_gbt_design;

architecture behavioral of alt_sx_gbt_design is

    -- GBT Tx   --
    signal gbt_txframeclk_s       : std_logic_vector(1 to NUM_LINKS);
    signal gbt_txreset_s          : std_logic_vector(1 to NUM_LINKS);
    signal gbt_txdata_s           : gbt_reg84_A(1 to NUM_LINKS);
    signal wb_txdata_s            : gbt_reg32_A(1 to NUM_LINKS);
    signal gbt_txclken_s          : std_logic_vector(1 to NUM_LINKS);
    -- GBT Rx   --
    signal gbt_rxframeclk_s       : std_logic_vector(1 to NUM_LINKS);
    signal gbt_rxreset_s          : std_logic_vector(1 to NUM_LINKS);
    signal gbt_rxready_s          : std_logic_vector(1 to NUM_LINKS);
    signal gbt_rxdata_s           : gbt_reg84_A(1 to NUM_LINKS);
    signal wb_rxdata_s            : gbt_reg32_A(1 to NUM_LINKS);
    signal gbt_rxclken_s          : std_logic_vector(1 to NUM_LiNKS);
    signal gbt_rxclkenLogic_s     : std_logic_vector(1 to NUM_LiNKS);
    -- MGT      --
    signal mgt_txwordclk_s        : std_logic_vector(1 to NUM_LINKS);
    signal mgt_rxwordclk_s        : std_logic_vector(1 to NUM_LINKS);
    signal mgt_txreset_s          : std_logic_vector(1 to NUM_LINKS);
    signal mgt_rxreset_s          : std_logic_vector(1 to NUM_LINKS);
    signal mgt_txready_s          : std_logic_vector(1 to NUM_LINKS);
    signal mgt_rxready_s          : std_logic_vector(1 to NUM_LINKS);
    signal mgt_headerflag_s       : std_logic_vector(1 to NUM_LINKS);
    signal mgt_devspecific_to_s   : mgtDeviceSpecific_i_R;
    signal mgt_devspecific_from_s : mgtDeviceSpecific_o_R;

begin

--signal debug
---------------------------------
MGT_TXRESET_DEBUG       <= mgt_txreset_s;
MGT_RXRESET_DEBUG       <= mgt_rxreset_s;
GBT_TXRESET_DEBUG       <= gbt_txreset_s;
GBT_RXRESET_DEBUG       <= gbt_rxreset_s;
---------------------------------

    gbt_inst : entity work.gbt_bank
        generic map(
            NUM_LINKS       => NUM_LINKS,
            TX_OPTIMIZATION => TX_OPTIMIZATION,
            RX_OPTIMIZATION => RX_OPTIMIZATION,
            TX_ENCODING     => GBT_FRAME,
            RX_ENCODING     => GBT_FRAME
        )
        port map(
            -- Resets --
            MGT_TXRESET_i        => mgt_txreset_s,
            MGT_RXRESET_i        => mgt_rxreset_s,
            GBT_TXRESET_i        => gbt_txreset_s,
            GBT_RXRESET_i        => gbt_rxreset_s,
            -- Clocks --
            MGT_CLK_i            => MGT_CLK_i,      --is a port
            GBT_TXFRAMECLK_i     => GBT_TXFRAMECLK_i,
            GBT_TXCLKEn_i        => GBT_TXCLKEN_i,
            GBT_RXFRAMECLK_i     => gbt_rxframeclk_s,
            GBT_RXCLKEn_i        => gbt_rxclken_s,
            MGT_TXWORDCLK_o      => mgt_txwordclk_s,
            MGT_RXWORDCLK_o      => mgt_rxwordclk_s,
            -- GBT TX Control --
            GBT_ISDATAFLAG_i     => GBT_ISDATAFLAG_i,  --??
            TX_ENCODING_SEL_i    => (others => '0'),  --by link is set to gbt frame.TX_ENCODING_SEL_i,
            -- GBT TX Status   --
            TX_PHALIGNED_o       => open,--not used
            TX_PHCOMPUTED_o      => open,--not used
            -- GBT RX Control --
            RX_ENCODING_SEL_i    => (others => '1'),  --used for dinamic change frame gbt or wide frame
            -- GBT RX Status   --
            GBT_RXREADY_o        => gbt_rxready_s,
            GBT_ISDATAFLAG_o     => GBT_ISDATAFLAG_o,
            GBT_ERRORDETECTED_o  => GBT_ERRORDETECTED_o,
            GBT_ERRORFLAG_o      => GBT_ERRORFLAG_o,
            -- MGT Control    --
            MGT_DEVSPECIFIC_i    => mgt_devspecific_to_s,
            MGT_RSTONBITSLIPEn_i => (others => '1'),
            MGT_RSTONEVEN_i      => (others => '1'),  -- set by default 
            -- MGT Status      --
            MGT_TXREADY_o        => mgt_txready_s,
            MGT_RXREADY_o        => mgt_rxready_s,
            MGT_DEVSPECIFIC_o    => mgt_devspecific_from_s,
            MGT_HEADERFLAG_o     => mgt_headerflag_s,
            MGT_RSTCNT_o         => open,  --set by default 
            --MGT_HEADERLOCKED_o       => open,
            -- Data   --
            GBT_TXDATA_i         => GBT_TXDATA_i,
            GBT_RXDATA_o         => GBT_RXDATA_o,
            WB_TXDATA_i          => WB_TXDATA_i ,
            WB_RXDATA_o          => WB_RXDATA_o
-----------------debug------------------
-- is needed to modify gbt_bank.vhd
--            TX_ANALOGRESET_DEBUG =>TX_ANALOGRESET_DEBUG, 
--            TX_DIGITALRESET_DEBUG=>TX_DIGITALRESET_DEBUG,
--            TX_READY_DEBUG       =>TX_READY_DEBUG,       
--            RX_ANALOGRESET_DEBUG =>RX_ANALOGRESET_DEBUG, 
--            RX_DIGITALRESET_DEBUG=>RX_DIGITALRESET_DEBUG,
--            RX_READY_DEBUG       =>RX_READY_DEBUG,       
--            RX_CAL_BUSY          =>RX_CAL_BUSY,       
--            RX_IS_LOCKEDTODATA   =>RX_IS_LOCKEDTODATA 

        );
    GBT_RXREADY_o <=  gbt_rxready_s ;
    -- Phase alingner RX clocks
    gbtBank_Clk_gen : for i in 1 to NUM_LINKS generate
        gbtBank_rxFrmClkPhAlgnr : entity work.gbt_rx_frameclk_phalgnr
            generic map(
            --    TX_OPTIMIZATION => TX_OPTIMIZATION,
            --    RX_OPTIMIZATION => RX_OPTIMIZATION,
                DIV_SIZE_CONFIG => 6,
                METHOD          => GATED_CLOCK,
                CLOCKING_SCHEME => CLOCKING_SCHEME
            )
            port map (
                RESET_I             => not(mgt_rxready_s(i)),
                RX_WORDCLK_I        => mgt_rxwordclk_s(i),
                FRAMECLK_I          => FRAMECLK_40MHZ,  --from outside
                RX_FRAMECLK_O       => gbt_rxframeclk_s(i),
                RX_CLKEn_o          => gbt_rxclkenLogic_s(i),
                SYNC_I              => mgt_headerflag_s(i),
                CLK_ALIGN_CONFIG    => (others => '0'),  --comes from outside??
                DEBUG_CLK_ALIGNMENT => open,
                PLL_LOCKED_O        => open,
                DONE_O              => open  
            );

        GBT_RXCLKENLOGIC_O(i) <= gbt_rxclkenLogic_s(i);
        GBT_RXFRAMECLK_O(i) <= gbt_rxframeclk_s(i);
        --GBT_TXFRAMECLK_O(i) <= gbt_txframeclk_s(i);
        MGT_TXWORDCLK_O(i)  <= mgt_txwordclk_s(i);
        MGT_RXWORDCLK_O(i)  <= mgt_rxwordclk_s(i);
        MGT_HEADERFLAG_O(i) <= mgt_headerflag_s(i);  --status  of rx header flag
        gbt_rxclken_s(i)    <= mgt_headerflag_s(i) when CLOCKING_SCHEME = FULL_MGTFREQ else '1';  --normally to '1'
        GBT_RXCLKEN_o(i)    <= mgt_headerflag_s(i) when CLOCKING_SCHEME = FULL_MGTFREQ else '1';  --normally to '1'
        --GBT_TXCLKEN_o(i)    <= gbt_txclken_s(i);

    end generate;

    -- Resets Scheme --
    gbtBank_rst_gen : for i in 1 to NUM_LINKS generate
        gbtBank_gbtBankRst : entity work.gbt_bank_reset
            generic map (
                INITIAL_DELAY => 1 * 40e0      --          * 1s 40e1 sim 40e6 normal
            )
            port map (
                GBT_CLK_I        => FRAMECLK_40MHZ,
                TX_FRAMECLK_I    => GBT_TXFRAMECLK_i(i),
                TX_CLKEN_I       => GBT_TXCLKEn_i(i),
                RX_FRAMECLK_I    => gbt_rxframeclk_s(i),
                RX_CLKEN_I       => gbt_rxclkenLogic_s(i),
                MGTCLK_I         => MGT_CLK_i,  --clock ref for transceivers
                GENERAL_RESET_I  => GBT_GENERAL_RESET_I,
                TX_RESET_I       =>'0',-- TX_RESET_ISSP_I(i), -- when controled by python script
                RX_RESET_I       => '0', -- idem
                MGT_TX_RESET_O   => mgt_txreset_s(i),
                MGT_RX_RESET_O   => mgt_rxreset_s(i),
                GBT_TX_RESET_O   => gbt_txreset_s(i),
                GBT_RX_RESET_O   => gbt_rxreset_s(i),
                MGT_TX_RSTDONE_I => mgt_txready_s(i),
                MGT_RX_RSTDONE_I => mgt_rxready_s(i)
            );
        -- Status signals
        GBT_LINK_TX_READY_O(i) <= mgt_txready_s(i);
        GBT_LINK_RX_READY_O(i) <= mgt_rxready_s(i);
        GBT_GBTRX_READY_O(i)   <= mgt_rxready_s(i) and gbt_rxready_s(i);
        GBT_LINK_READY_O(i)    <= mgt_txready_s(i) and mgt_rxready_s(i);
        GBT_GBTTX_READY_O(i)   <= not(gbt_txreset_s(i));

    end generate;

    -- MGT reconf
    mgt_devspecific_to_s.reconf_reset                <= '1';
    mgt_devspecific_to_s.reconf_clk                  <= '0';
    mgt_devspecific_to_s.reconf_avmm_addr            <= (others => '0');
    mgt_devspecific_to_s.reconf_avmm_read            <= '0';
    mgt_devspecific_to_s.reconf_avmm_write           <= '0';
    mgt_devspecific_to_s.reconf_avmm_writedata       <= (others => '0');
    -- Otput of the reconf interface yet not used
    --mgt_devspecific_from_s.reconf_avmm_readdata;
    --mgt_devspecific_from_s.reconf_avmm_waitrequest;
    -- PLL Reconf
    mgt_devspecific_to_s.txpll_reconf_reset          <= '1';
    mgt_devspecific_to_s.txpll_reconf_clk            <= '0';
    mgt_devspecific_to_s.txpll_reconf_avmm_addr      <= (others => '0');
    mgt_devspecific_to_s.txpll_reconf_avmm_read      <= '0';
    mgt_devspecific_to_s.txpll_reconf_avmm_write     <= '0';
    mgt_devspecific_to_s.txpll_reconf_avmm_writedata <= (others => '0');
    -- Otput of the reconf interface yet not used
    --mgt_devspecific_from_s.txpll_reconf_avmm_readdata;
    --mgt_devspecific_from_s.txpll_reconf_avmm_waitrequest;

    gbtBank_mgt_gen : for i in 1 to NUM_LINKS generate
        --By default, control from python script.
        mgt_devspecific_to_s.loopBack(i)     <= GBT_BANK_LOOPBACK;
        mgt_devspecific_to_s.tx_polarity(i)  <= '1';
        mgt_devspecific_to_s.rx_polarity(i)  <= '1';
        -- Serial lanes
        mgt_devspecific_to_s.rxSerialData(i) <= GBT_MGT_RX(i);
        GBT_MGT_TX(i)                        <= mgt_devspecific_from_s.txSerialData(i);
    end generate;

end behavioral;
