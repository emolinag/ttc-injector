library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;

-- Custom libraries and packages:
use work.gbt_bank_package.all;
use work.vendor_specific_gbt_bank_package.all;
use work.gbt_exampledesign_package.all;


entity ttc_injector_top is
  generic(
    NUM_LINKS                 : integer := 1;
    PATTERN_SEL               : std_logic_vector(1 downto 0):="10";
    CLOCKING_SCHEME           : integer range 0 to 1 := BC_CLOCK
  );
  port(
    refclk_i        : in std_logic;
    mgtclk_i        : in std_logic;
    clk_100Mhz_i    : in std_logic;
    user_pb         : in std_logic;
    user_dipsw      : in std_logic_vector(2 downto 0);
    ttc_recover_clk : out std_logic;
    ttc_bcid        : out std_logic_vector(11 downto 0);
    ttc_bcid_valid  : out std_logic;
    ttc_bcr         : out std_logic;
    gbt_mgt_rx      : in std_logic_vector(1 to NUM_LINKS);
    gbt_mgt_tx      : out std_logic_vector(1 to NUM_LINKS)

  );
end ttc_injector_top;

architecture behavior of ttc_injector_top is

  signal mgt_clk_s                   : std_logic;  --is a port
  signal gbt_isdataflag_rx_s         : std_logic_vector(1 to NUM_LINKS);  --rx data is valid
  signal gbt_isdataflag_tx_s         : std_logic_vector(1 to NUM_LINKS);  --tx data is valid
  signal gbt_errordetected_s         : std_logic_vector(1 to NUM_LINKS);
  signal gbt_errorflag_s             : gbt_reg84_A(1 to NUM_LINKS);
  signal gbt_txdata_s                : gbt_reg84_A(1 to NUM_LINKS);
  signal gbt_rxdata_s                : gbt_reg84_A(1 to NUM_LINKS);
  signal wb_txdata_s                 : gbt_reg32_A (1 to NUM_LINKS);
  signal wb_rxdata_s                 : gbt_reg32_A (1 to NUM_LINKS);
  signal frameclk_40mhz              : std_logic;  
  signal gbt_general_reset_s         : std_logic;
  signal general_reset_button        : std_logic;
  signal user_pb_s                   : std_logic;
  signal gbtbank_rx_frameclk_ready_s : std_logic_vector(1 to NUM_LINKS);
  signal gbt_rxframeclk_s            : std_logic_vector(1 to NUM_LINKS);
  signal gbt_txframeclk_s            : std_logic_vector(1 to NUM_LINKS);
  signal mgt_txwordclk_s             : std_logic_vector(1 to NUM_LINKS);
  signal mgt_rxwordclk_s             : std_logic_vector(1 to NUM_LINKS);
  signal mgt_headerflag_s            : std_logic_vector(1 to NUM_LINKS);
  signal gbt_link_tx_ready_s         : std_logic_vector(1 to NUM_LINKS);
  signal gbt_link_rx_ready_s         : std_logic_vector(1 to NUM_LINKS);
  signal gbt_gbtrx_ready_s           : std_logic_vector(1 to NUM_LINKS);
  signal gbt_link_ready_s            : std_logic_vector(1 to NUM_LINKS);
  signal gbt_gbttx_ready_s           : std_logic_vector(1 to NUM_LINKS);
  signal gbt_rxclken_s               : std_logic_vector(1 to NUM_LINKS);
  signal gbt_txclken_s               : std_logic_vector(1 to NUM_LINKS);
  signal gbt_tx_reset_s              : std_logic_vector(1 to NUM_LINKS);
  signal gbt_rxready_s               : std_logic_vector(1 to NUM_LINKS);
  signal gbt_rxclkenlogic_s          : std_logic_vector(1 to NUM_LINKS);
  signal pattern_gen_rst             : std_logic_vector(1 to NUM_LINKS);
  signal no_valid_data               : std_logic_vector(1 to NUM_LINKS);
  signal reset_after_250us           : std_logic;


  signal rx_cal_busy             : std_logic_vector(1 to NUM_LINKS);
  signal rx_is_lockedtodata      : std_logic_vector(1 to NUM_LINKS);
  signal user_dipsw_s            : std_logic_vector(2 downto 0);
  signal gbt_bank_loopback       : std_logic;

begin

    frameclk_40mhz <= refclk_i;
    mgt_clk_s      <= mgtclk_i;

    --arrange output
    ttc_recover_clk<=gbt_rxframeclk_s(1);
    ttc_bcid       <=gbt_rxdata_s(1)(12 downto 1);
    ttc_bcid_valid <=gbt_isdataflag_rx_s(1);
    ttc_bcr        <=gbt_rxdata_s(1)(0);

    -- Data pattern generator
    dataGenEn_output_gen : for i in 1 to NUM_LINKS generate

    pattern_gen_rst(i) <= gbt_tx_reset_s(i); --add something for issp

        gbtBank2_pattGen : entity work.gbt_pattern_generator
            generic map(
                CLOCKING_SCHEME => CLOCKING_SCHEME
            )
            port map (
                GENERAL_RST_I                       => gbt_general_reset_s --Connect to general resert -- GBTBANK_GENERAL_RESET_I or GBTBANK_MANUAL_RESET_TX_I, add a reset module
                ,RESET_I                            => pattern_gen_rst(i)  --Gbt_tx_reset_s(i)       --Connect to gbt_tx_reset
                ,TX_FRAMECLK_I                      => frameclk_40mhz      --To 40Mhz -- FRAMECLK_40MHZ,  top input
                ,TX_WORDCLK_I                       => mgt_txwordclk_s(i)  --(comes from gbt_bank) --mgt_txwordclk_s(i),
                ,TX_FRAMECLK_O                      => gbt_txframeclk_s(i) --Connect gbt_tx_frameclk_i from gbt_bank --gbt_txframeclk_s(i),
                ,TX_CLKEN_o                         => gbt_txclken_s(i)    --Connect to gbt_tx_clken_i from gbt_bank --gbt_txclken_s(i),
                ,TX_ENCODING_SEL_I                  => "00"
                ,TEST_PATTERN_SEL_I                 => user_dipsw_s(1 downto 0) --add something for issp
                ,STATIC_PATTERN_SCEC_I              => "00"
                ,STATIC_PATTERN_DATA_I              => x"000BABEAC1DACDCFFFFF"
                ,STATIC_PATTERN_EXTRADATA_WIDEBUS_I => x"BEEFCAFE"
                ,TX_VALID_O                         => gbt_isdataflag_tx_s(i)
                ,TX_DATA_O                          => gbt_txdata_s(i)    --To the gbt_data ports
                ,TX_EXTRA_DATA_WIDEBUS_O            => wb_txdata_s(i)     --To the gbt_data ports
            );

    end generate;

    -- Data pattern checker
    gbtBank_patCheck_gen : for i in 1 to NUM_LINKS generate
        gbtBank_pattCheck : entity work.gbt_pattern_checker
            port map (
                RESET_I                               => gbt_general_reset_s    -- Connet to global reset
                ,RX_FRAMECLK_I                        => gbt_rxframeclk_s(i)    -- Connect to gbt_rxframeclk from phase align
                ,RX_CLKEN_I                           => gbt_rxclkenlogic_s(i)
                ,RX_DATA_I                            => gbt_rxdata_s(i)        -- gbt_rxdata_s(i),
                ,RX_EXTRA_DATA_WIDEBUS_I              => wb_rxdata_s(i)         -- wb_rxdata_s(i),
                ,GBT_RX_READY_I                       => gbt_rxready_s(i)       -- gbt_rxready_s(i),
                ,RX_ENCODING_SEL_I                    => "00"
                ,TEST_PATTERN_SEL_I                   => user_dipsw_s(1 downto 0)
                ,STATIC_PATTERN_SCEC_I                => "00"
                ,STATIC_PATTERN_DATA_I                => x"000BABEAC1DACDCFFFFF"
                ,STATIC_PATTERN_EXTRADATA_WIDEBUS_I   => x"BEEFCAFE"
                ,RESET_GBTRXREADY_LOST_FLAG_I         =>'0'                     --(others =>'0') -- Both signals useul when ISSP used 
                ,RESET_DATA_ERRORSEEN_FLAG_I          =>'0'                     --(othres =>'0') -- idem 
                ,GBTRXREADY_LOST_FLAG_O               => open -- 
                ,RXDATA_ERRORSEEN_FLAG_O              => open -- 
                ,RXEXTRADATA_WIDEBUS_ERRORSEEN_FLAG_O => open -- 
            );

    end generate;

    --Instance of top design of gbt-bank
    inst_alt_sx_gbt_design : entity work.alt_sx_gbt_design
        generic map(
           NUM_LINKS         => NUM_LINKS
           ,TX_OPTIMIZATION  => LATENCY_OPTIMIZED
           ,RX_OPTIMIZATION  => LATENCY_OPTIMIZED
           ,CLOCKING_SCHEME => CLOCKING_SCHEME
        )
        port map (
            MGT_CLK_i                    => mgt_clk_s       
            ,GBT_ISDATAFLAG_i            => gbt_isdataflag_tx_s 
            ,GBT_ISDATAFLAG_o            => gbt_isdataflag_rx_s 
            ,GBT_ERRORDETECTED_o         => gbt_errordetected_s 
            ,GBT_ERRORFLAG_o             => gbt_errorflag_s
            ,GBT_TXDATA_i                => gbt_txdata_s
            ,GBT_RXDATA_o                => gbt_rxdata_s
            ,WB_TXDATA_i                 => wb_txdata_s 
            ,WB_RXDATA_o                 => wb_rxdata_s 
            ,FRAMECLK_40MHZ              => frameclk_40mhz  
            ,GBT_GENERAL_RESET_I         => gbt_general_reset_s 
            ,GBTBANK_RX_FRAMECLK_READY_O => gbtbank_rx_frameclk_ready_s  
            ,GBT_RXFRAMECLK_O            => gbt_rxframeclk_s
            ,GBT_TXFRAMECLK_i            => gbt_txframeclk_s
            ,MGT_TXWORDCLK_O             => mgt_txwordclk_s 
            ,MGT_RXWORDCLK_O             => mgt_rxwordclk_s 
            ,MGT_HEADERFLAG_O            => mgt_headerflag_s
            ,GBT_LINK_TX_READY_O         => gbt_link_tx_ready_s 
            ,GBT_LINK_RX_READY_O         => gbt_link_rx_ready_s 
            ,GBT_GBTRX_READY_O           => gbt_gbtrx_ready_s
            ,GBT_LINK_READY_O            => gbt_link_ready_s 
            ,GBT_GBTTX_READY_O           => gbt_gbttx_ready_s
            ,GBT_MGT_RX                  => gbt_mgt_rx 
            ,GBT_MGT_TX                  => gbt_mgt_tx 
            ,GBT_RXCLKEN_o               => gbt_rxclken_s
            ,GBT_RXREADY_o               => gbt_rxready_s
            ,GBT_RXCLKENLOGIC_o          => gbt_rxclkenlogic_s
            ,GBT_TXCLKEn_i               => gbt_txclken_s
            ,GBT_BANK_LOOPBACK           => gbt_bank_loopback 
        );

  gbt_tx_reset_s <= not gbt_gbttx_ready_s; --to reset the patter gen
  

  inst_debouncer_reset : entity work.debounce
    port map(
      clk      => clk_100Mhz_i
      ,button  => user_pb --input signal to be debounced
      ,result  => user_pb_s--debounced signal
    );

----  reset ---
gbt_general_reset_s <=  '1' when user_pb_s = '0' else '0';

  inst_debouncer_dipsw0 : entity work.debounce
    port map(
      clk      => clk_100Mhz_i
      ,button  => user_dipsw(0) 
      ,result  => user_dipsw_s(0)
    );

   inst_debouncer_dipsw1 : entity work.debounce
    port map(
      clk      => clk_100Mhz_i
      ,button  => user_dipsw(1)
      ,result  => user_dipsw_s(1)   );
 
   inst_debouncer_dipsw2 : entity work.debounce
    port map(
      clk      => clk_100Mhz_i
      ,button  => user_dipsw(2)
      ,result  => user_dipsw_s(2)   );
 
  gbt_bank_loopback <=  user_dipsw_s(2);

end behavior;
