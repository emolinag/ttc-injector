library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;

-- Custom libraries and packages:
use work.gbt_bank_package.all;
use work.vendor_specific_gbt_bank_package.all;
use work.gbt_exampledesign_package.all;

library issp;

entity ttc_injector_issp is
  generic(
    NUM_LINKS                 : integer := 1;
    PATTERN_SEL               : std_logic_vector(1 downto 0):="10";
    CLOCKING_SCHEME           : integer range 0 to 1 := BC_CLOCK
  );
  port(
    refclk_i        : in std_logic;
    mgtclk_i        : in std_logic;
    clk_100Mhz_i    : in std_logic;
    user_pb         : in std_logic;
    ttc_recover_clk : out std_logic;
    ttc_bcid        : out std_logic_vector(11 downto 0);
    ttc_bcid_valid  : out std_logic;
    ttc_bcr         : out std_logic;
    gbt_mgt_rx      : in std_logic_vector(1 to NUM_LINKS);
    gbt_mgt_tx      : out std_logic_vector(1 to NUM_LINKS)

  );
end ttc_injector_issp;

architecture behavior of ttc_injector_issp is

  signal mgt_clk_s                   : std_logic;  --is a port
  signal gbt_isdataflag_rx_s         : std_logic_vector(1 to NUM_LINKS);  --rx data is valid
  signal gbt_isdataflag_tx_s         : std_logic_vector(1 to NUM_LINKS);  --tx data is valid
  signal gbt_errordetected_s         : std_logic_vector(1 to NUM_LINKS);
  signal gbt_errorflag_s             : gbt_reg84_A(1 to NUM_LINKS);
  signal gbt_txdata_s                : gbt_reg84_A(1 to NUM_LINKS);
  signal gbt_rxdata_s                : gbt_reg84_A(1 to NUM_LINKS);
  signal wb_txdata_s                 : gbt_reg32_A (1 to NUM_LINKS);
  signal wb_rxdata_s                 : gbt_reg32_A (1 to NUM_LINKS);
  signal frameclk_40mhz              : std_logic;  
  signal gbt_general_reset_s         : std_logic;
  signal gbt_general_reset_issp      : std_logic;
  signal general_reset_button        : std_logic;
  signal user_pb_s                   : std_logic;
  signal gbtbank_rx_frameclk_ready_s : std_logic_vector(1 to NUM_LINKS);
  signal gbt_rxframeclk_s            : std_logic_vector(1 to NUM_LINKS);
  signal gbt_txframeclk_s            : std_logic_vector(1 to NUM_LINKS);
  signal mgt_txwordclk_s             : std_logic_vector(1 to NUM_LINKS);
  signal mgt_rxwordclk_s             : std_logic_vector(1 to NUM_LINKS);
  signal mgt_headerflag_s            : std_logic_vector(1 to NUM_LINKS);
  signal gbt_link_tx_ready_s         : std_logic_vector(1 to NUM_LINKS);
  signal gbt_link_rx_ready_s         : std_logic_vector(1 to NUM_LINKS);
  signal gbt_gbtrx_ready_s           : std_logic_vector(1 to NUM_LINKS);
  signal gbt_link_ready_s            : std_logic_vector(1 to NUM_LINKS);
  signal gbt_gbttx_ready_s           : std_logic_vector(1 to NUM_LINKS);
  signal gbt_rxclken_s               : std_logic_vector(1 to NUM_LINKS);
  signal gbt_txclken_s               : std_logic_vector(1 to NUM_LINKS);
  signal gbt_tx_reset_s              : std_logic_vector(1 to NUM_LINKS);
  signal gbt_rxready_s               : std_logic_vector(1 to NUM_LINKS);
  signal gbt_rxclkenlogic_s          : std_logic_vector(1 to NUM_LINKS);
  signal pattern_gen_rst             : std_logic_vector(1 to NUM_LINKS);
  signal no_valid_data               : std_logic_vector(1 to NUM_LINKS);
  signal reset_after_250us           : std_logic;

  signal done_from_rxbitslipcontrol_s_bind: std_logic_vector(1 to NUM_LINKS);
  signal pattern_sel_s                       : std_logic_vector(1 downto 0);

  signal mgt_txreset_debug       : std_logic_vector(1 to NUM_LINKS);
  signal mgt_rxreset_debug       : std_logic_vector(1 to NUM_LINKS);
  signal gbt_txreset_debug       : std_logic_vector(1 to NUM_LINKS);
  signal gbt_rxreset_debug       : std_logic_vector(1 to NUM_LINKS);
  signal tx_analogreset_debug    : std_logic_vector(1 to NUM_LINKS);
  signal tx_digitalreset_debug   : std_logic_vector(1 to NUM_LINKS);
  signal tx_ready_debug          : std_logic_vector(1 to NUM_LINKS);
  signal rx_analogreset_debug    : std_logic_vector(1 to NUM_LINKS);
  signal rx_digitalreset_debug   : std_logic_vector(1 to NUM_LINKS);
  signal rx_ready_debug          : std_logic_vector(1 to NUM_LINKS);
  signal rx_cal_busy             : std_logic_vector(1 to NUM_LINKS);
  signal rx_is_lockedtodata      : std_logic_vector(1 to NUM_LINKS);
  signal tx_reset_issp_i         : std_logic_vector(1 to NUM_LINKS);
  signal tx_reset_issp_s         : std_logic;

  signal gbt_bank_loopback       : std_logic; 

  signal  nrst_general : std_logic;
  signal  s_valid_40      : std_logic;
  signal  s_freq_40       : std_logic_vector(7 downto 0);
  signal  s_valid_240      : std_logic;
  signal  s_freq_240       : std_logic_vector(7 downto 0);

begin

    frameclk_40mhz <= refclk_i;
    mgt_clk_s      <= mgtclk_i;

    --arrange output
    ttc_recover_clk<=gbt_rxframeclk_s(1);
    ttc_bcid       <=gbt_rxdata_s(1)(12 downto 1);
    ttc_bcid_valid <=gbt_isdataflag_rx_s(1);
    ttc_bcr        <=gbt_rxdata_s(1)(0);

    -- Data pattern generator
    dataGenEn_output_gen : for i in 1 to NUM_LINKS generate

    pattern_gen_rst(i) <= gbt_tx_reset_s(i); --add something for issp

        gbtBank2_pattGen : entity work.gbt_pattern_generator
            generic map(
                CLOCKING_SCHEME => CLOCKING_SCHEME
            )
            port map (
                GENERAL_RST_I                       => gbt_general_reset_s --Connect to general resert -- GBTBANK_GENERAL_RESET_I or GBTBANK_MANUAL_RESET_TX_I, add a reset module
                ,RESET_I                            => pattern_gen_rst(i)  --Gbt_tx_reset_s(i)       --Connect to gbt_tx_reset
                ,TX_FRAMECLK_I                      => frameclk_40mhz      --To 40Mhz -- FRAMECLK_40MHZ,  top input
                ,TX_WORDCLK_I                       => mgt_txwordclk_s(i)  --(comes from gbt_bank) --mgt_txwordclk_s(i),
                ,TX_FRAMECLK_O                      => gbt_txframeclk_s(i) --Connect gbt_tx_frameclk_i from gbt_bank --gbt_txframeclk_s(i),
                ,TX_CLKEN_o                         => gbt_txclken_s(i)    --Connect to gbt_tx_clken_i from gbt_bank --gbt_txclken_s(i),
                ,TX_ENCODING_SEL_I                  => "00"
                ,TEST_PATTERN_SEL_I                 => pattern_sel_s --add something for issp
                ,STATIC_PATTERN_SCEC_I              => "00"
                ,STATIC_PATTERN_DATA_I              => x"000BABEAC1DACDCFFFFF"
                ,STATIC_PATTERN_EXTRADATA_WIDEBUS_I => x"BEEFCAFE"
                ,TX_VALID_O                         => gbt_isdataflag_tx_s(i)
                ,TX_DATA_O                          => gbt_txdata_s(i)    --To the gbt_data ports
                ,TX_EXTRA_DATA_WIDEBUS_O            => wb_txdata_s(i)     --To the gbt_data ports
            );

    end generate;

    -- Data pattern checker
    gbtBank_patCheck_gen : for i in 1 to NUM_LINKS generate
        gbtBank_pattCheck : entity work.gbt_pattern_checker
            port map (
                RESET_I                               => gbt_general_reset_s    -- Connet to global reset
                ,RX_FRAMECLK_I                        => gbt_rxframeclk_s(i)    -- Connect to gbt_rxframeclk from phase align
                ,RX_CLKEN_I                           => gbt_rxclkenlogic_s(i)
                ,RX_DATA_I                            => gbt_rxdata_s(i)        -- gbt_rxdata_s(i),
                ,RX_EXTRA_DATA_WIDEBUS_I              => wb_rxdata_s(i)         -- wb_rxdata_s(i),
                ,GBT_RX_READY_I                       => gbt_rxready_s(i)       -- gbt_rxready_s(i),
                ,RX_ENCODING_SEL_I                    => "00"
                ,TEST_PATTERN_SEL_I                   => PATTERN_SEL
                ,STATIC_PATTERN_SCEC_I                => "00"
                ,STATIC_PATTERN_DATA_I                => x"000BABEAC1DACDCFFFFF"
                ,STATIC_PATTERN_EXTRADATA_WIDEBUS_I   => x"BEEFCAFE"
                ,RESET_GBTRXREADY_LOST_FLAG_I         =>'0'                     --(others =>'0') -- Both signals useul when ISSP used 
                ,RESET_DATA_ERRORSEEN_FLAG_I          =>'0'                     --(othres =>'0') -- idem 
                ,GBTRXREADY_LOST_FLAG_O               => open -- 
                ,RXDATA_ERRORSEEN_FLAG_O              => open -- 
                ,RXEXTRADATA_WIDEBUS_ERRORSEEN_FLAG_O => open -- 
            );

    tx_reset_issp_i(i) <= '0';
    end generate;

    --Instance of top design of gbt-bank
    inst_alt_sx_gbt_design : entity work.alt_sx_gbt_design
        generic map(
           NUM_LINKS         => NUM_LINKS
           ,TX_OPTIMIZATION  => LATENCY_OPTIMIZED
           ,RX_OPTIMIZATION  => LATENCY_OPTIMIZED
           ,CLOCKING_SCHEME => CLOCKING_SCHEME
        )
        port map (
            MGT_CLK_i                    => mgt_clk_s       
            ,GBT_ISDATAFLAG_i            => gbt_isdataflag_tx_s 
            ,GBT_ISDATAFLAG_o            => gbt_isdataflag_rx_s 
            ,GBT_ERRORDETECTED_o         => gbt_errordetected_s 
            ,GBT_ERRORFLAG_o             => gbt_errorflag_s
            ,GBT_TXDATA_i                => gbt_txdata_s
            ,GBT_RXDATA_o                => gbt_rxdata_s
            ,WB_TXDATA_i                 => wb_txdata_s 
            ,WB_RXDATA_o                 => wb_rxdata_s 
            ,FRAMECLK_40MHZ              => frameclk_40mhz  
            ,GBT_GENERAL_RESET_I         => gbt_general_reset_s 
            ,GBTBANK_RX_FRAMECLK_READY_O => gbtbank_rx_frameclk_ready_s  
            ,GBT_RXFRAMECLK_O            => gbt_rxframeclk_s
            ,GBT_TXFRAMECLK_i            => gbt_txframeclk_s
            ,MGT_TXWORDCLK_O             => mgt_txwordclk_s 
            ,MGT_RXWORDCLK_O             => mgt_rxwordclk_s 
            ,MGT_HEADERFLAG_O            => mgt_headerflag_s
            ,GBT_LINK_TX_READY_O         => gbt_link_tx_ready_s 
            ,GBT_LINK_RX_READY_O         => gbt_link_rx_ready_s 
            ,GBT_GBTRX_READY_O           => gbt_gbtrx_ready_s
            ,GBT_LINK_READY_O            => gbt_link_ready_s 
            ,GBT_GBTTX_READY_O           => gbt_gbttx_ready_s
            ,GBT_MGT_RX                  => gbt_mgt_rx 
            ,GBT_MGT_TX                  => gbt_mgt_tx 
            ,GBT_RXCLKEN_o               => gbt_rxclken_s
            ,GBT_RXREADY_o               => gbt_rxready_s
            ,GBT_RXCLKENLOGIC_o          => gbt_rxclkenlogic_s
            ,GBT_TXCLKEn_i               => gbt_txclken_s
            ,GBT_BANK_LOOPBACK           => gbt_bank_loopback 
-----------------debug------------------
            ,MGT_TXRESET_DEBUG          => mgt_txreset_debug
            ,MGT_RXRESET_DEBUG          => mgt_rxreset_debug
            ,GBT_TXRESET_DEBUG          => gbt_txreset_debug
            ,GBT_RXRESET_DEBUG          => gbt_rxreset_debug
            ,TX_ANALOGRESET_DEBUG       => tx_analogreset_debug
            ,TX_DIGITALRESET_DEBUG      => tx_digitalreset_debug
            ,TX_READY_DEBUG             => tx_ready_debug
            ,RX_ANALOGRESET_DEBUG       => rx_analogreset_debug
            ,RX_DIGITALRESET_DEBUG      => rx_digitalreset_debug
            ,RX_READY_DEBUG             => rx_ready_debug        
            ,RX_CAL_BUSY                => rx_cal_busy
            ,RX_IS_LOCKEDTODATA         => rx_is_lockedtodata

        );

  gbt_tx_reset_s <= not gbt_gbttx_ready_s; --to reset the patter gen
  
-----  debug zone -----
--Instances module to read clocks

nrst_general <= not gbt_general_reset_s;

inst_measure_freq_40: entity  work.measure_freq 
  port map( 
     clk_ref_i      => clk_100Mhz_i   
     ,clk_measure_i => frameclk_40mhz 
     ,nrst_i        => nrst_general   
     ,valid_freq_o  => s_valid_40   
     ,freq_o        => s_freq_40    
  );

inst_measure_freq_240: entity  work.measure_freq 
  port map( 
     clk_ref_i      => clk_100Mhz_i   
     ,clk_measure_i => mgt_clk_s 
     ,nrst_i        => nrst_general   
     ,valid_freq_o  => s_valid_240   
     ,freq_o        => s_freq_240    
  );
-------------------------------------

  inst_debouncer : entity work.debounce
    port map(
      clk      => clk_100Mhz_i
      ,button  => user_pb --input signal to be debounced
      ,result  => user_pb_s--debounced signal
    );
----  reset ---
general_reset_button <= '1' when user_pb_s = '0' else '0';
gbt_general_reset_s <= general_reset_button or gbt_general_reset_issp;


--gbt_bank_loopback <= '1';
  
    inst_issp : entity issp.issp
        port map (
             probe(0)              =>  mgt_txreset_debug(1)  
            ,probe(1)              =>  mgt_rxreset_debug(1)  
            ,probe(2)              =>  gbt_txreset_debug(1)  
            ,probe(3)              =>  gbt_rxreset_debug(1)  
            ,probe(4)              =>  tx_analogreset_debug(1)   
            ,probe(5)              =>  tx_digitalreset_debug(1)  
            ,probe(6)              =>  tx_ready_debug(1)         
            ,probe(7)              =>  rx_analogreset_debug(1)   
            ,probe(8)              =>  rx_digitalreset_debug(1)  
            ,probe(9)              =>  rx_ready_debug(1)         
            ,probe(17 downto 10)   =>  s_freq_40         
            ,probe(18)             =>  s_valid_40         
            ,probe(26 downto 19)   =>  s_freq_240         
	    ,probe(27)             =>  s_valid_240         
            ,probe(28)             => rx_cal_busy(1)
            ,probe(29)             => rx_is_lockedtodata(1)
            ,probe(30)             => gbt_isdataflag_tx_s(1)  
            ,probe(31)             => gbt_isdataflag_rx_s(1)  
            ,probe(32)             => gbt_errordetected_s(1) 
            ,probe(33)             => gbt_link_tx_ready_s(1) 
            ,probe(34)             => gbt_link_rx_ready_s(1)
            ,probe(118 downto 35)  => gbt_txdata_s(1)
            ,probe(202 downto 119) => gbt_rxdata_s(1)
            ,probe(347 downto 203) => (others => '0') --gbt_isdataflag_tx_s(2)  
            ,source(0)             => gbt_general_reset_issp  
	    ,source(1)             => gbt_bank_loopback
            ,source(3 downto 2)    => pattern_sel_s   
 
        ); 

end behavior;
