vcom -work work $sources/gbt_bank/altera_s10/alt_ax_gbt_bank_package.vhd

vcom -work work \
$sources/gbt-fpga/gbt_bank/core_sources/gbt_bank_package.vhd\
$sources/gbt-fpga/gbt_bank/core_sources/gbt_tx/gbt_tx_gearbox_std_rdwrctrl.vhd

##add dpram
vcom -work work \
$sources/gbt_bank/altera_s10/gbt_tx/alt_sx_gbt_tx_gearbox_std_dpram.vhd\
$sources/gbt_bank/altera_s10/gbt_rx/alt_sx_gbt_rx_gearbox_std_dpram.vhd


vcom -novopt -2008 -work work \
$sources/gbt-fpga/gbt_bank/core_sources/gbt_tx/gbt_tx_scrambler_16bit.vhd\
$sources/gbt-fpga/gbt_bank/core_sources/gbt_tx/gbt_tx_scrambler_21bit.vhd\
$sources/gbt-fpga/gbt_bank/core_sources/gbt_tx/gbt_tx_scrambler.vhd\
$sources/gbt_tx_encoder_gbtframe_polydiv.vhd\
$sources/gbt-fpga/gbt_bank/core_sources/gbt_tx/gbt_tx_encoder_gbtframe_intlver.vhd\
$sources/gbt_tx_encoder_gbtframe_rsencode.vhd\
$sources/gbt-fpga/gbt_bank/core_sources/gbt_tx/gbt_tx_encoder.vhd\
$sources/gbt-fpga/gbt_bank/core_sources/gbt_tx/gbt_tx_gearbox_std.vhd\
$sources/gbt-fpga/gbt_bank/core_sources/gbt_tx/gbt_tx.vhd\
$sources/gbt-fpga/gbt_bank/core_sources/gbt_tx/gbt_tx_gearbox_phasemon.vhd\
$sources/gbt-fpga/gbt_bank/core_sources/gbt_tx/gbt_tx_gearbox_latopt.vhd\
$sources/gbt-fpga/gbt_bank/core_sources/gbt_tx/gbt_tx_gearbox.vhd\
$sources/gbt-fpga/gbt_bank/core_sources/gbt_rx/gbt_rx_descrambler_16bit.vhd\
$sources/gbt-fpga/gbt_bank/core_sources/gbt_rx/gbt_rx_descrambler_21bit.vhd\
$sources/gbt-fpga/gbt_bank/core_sources/gbt_rx/gbt_rx_descrambler.vhd\
$sources/gbt_rx_gearbox_std_rdctrl.vhd\
$sources/gbt-fpga/gbt_bank/core_sources/gbt_rx/gbt_rx_decoder_gbtframe_errlcpoly.vhd\
$sources/gbt-fpga/gbt_bank/core_sources/gbt_rx/gbt_rx_decoder_gbtframe_syndrom.vhd\
$sources/gbt-fpga/gbt_bank/core_sources/gbt_rx/gbt_rx_decoder_gbtframe_lmbddet.vhd\
$sources/gbt-fpga/gbt_bank/core_sources/gbt_rx/gbt_rx_decoder_gbtframe_rs2errcor.vhd\
$sources/gbt-fpga/gbt_bank/core_sources/gbt_rx/gbt_rx_decoder_gbtframe_elpeval.vhd\
$sources/gbt-fpga/gbt_bank/core_sources/gbt_rx/gbt_rx_decoder_gbtframe_chnsrch.vhd\
$sources/gbt-fpga/gbt_bank/core_sources/gbt_rx/gbt_rx_decoder_gbtframe_deintlver.vhd\
$sources/gbt-fpga/gbt_bank/core_sources/gbt_rx/gbt_rx_decoder_gbtframe_rsdec.vhd\
$sources/gbt-fpga/gbt_bank/core_sources/gbt_rx/gbt_rx_gearbox_latopt.vhd\
$sources/gbt-fpga/gbt_bank/core_sources/gbt_rx/gbt_rx_gearbox_std.vhd\
$sources/gbt-fpga/gbt_bank/core_sources/gbt_rx/gbt_rx_gearbox.vhd\
$sources/gbt-fpga/gbt_bank/core_sources/gbt_rx/gbt_rx_decoder.vhd\
$sources/gbt-fpga/gbt_bank/core_sources/gbt_rx/gbt_rx.vhd\
$sources/gbt-fpga/gbt_bank/core_sources/mgt/mgt_framealigner_pattsearch.vhd\
$sources/gbt-fpga/gbt_bank/core_sources/mgt/mgt_bitslipctrl.vhd

##add mgt expecific architecture file

vcom -novopt -2008 -work work \
$sources/gbt_bank/altera_s10/mgt/alt_sx_mgt_resetctrl.vhd\
$sources/gbt_bank/altera_s10/mgt/alt_mgt_txpll.vhd\
$sources/gbt_bank/altera_s10/mgt/alt_sx_mgt_txpll.vhd\
$sources/gbt_bank/altera_s10/mgt/alt_sx_mgt.vhd

vcom -2008 -work work \
$sources/gbt-fpga/gbt_bank/core_sources/gbt_bank.vhd
