library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;

use work.gbt_bank_package.all;
use work.gbt_exampledesign_package.all;

--add package of numlinks def
entity ttc_injector_rst_checker is
  generic(
    NUM_LINKS : integer:=1 --definition
  );
  port(
    MGT_TXRESET_I              : in std_logic;   
    MGT_RXRESET_I              : in std_logic;    
    GBT_TXRESET_I              : in std_logic;    
    GBT_RXRESET_I              : in std_logic;     
    MGT_CKL_I                  : in std_logic;    
    TX_FRAMECLK_I              : in std_logic;       
    RX_FRAMECLK_I              : in std_logic;   
    TX_ANALOG                  : in std_logic;  
    TX_DIGITAL                 : in std_logic;  
    TX_READY                   : in std_logic;  
    RX_ANALOG                  : in std_logic;  
    RX_DIGITAL                 : in std_logic;  
    RX_READY                   : in std_logic; 
    DONE_FROM_RXBITSLIPCONTROL : in std_logic;
    RX_CLKEN_I                 : in std_logic;   
    TX_CLKEN_I                 : in std_logic   
  );
end ttc_injector_rst_checker;

architecture behavioral of ttc_injector_rst_checker is

  signal flag : boolean:=true;

begin

-- Process that check reset sequence, step by step.
proc_rst_checK: process
  variable count : integer range 0 to 410001;--hypotesis that reset is not more than one minut simulation!
  begin

    wait until MGT_TXRESET_I = '1' and MGT_RXRESET_I = '1' and GBT_TXRESET_I = '1' and GBT_RXRESET_I ='1';
  
    report "-------------    Reseting the GBT-BANK  -------------";
  
    -- Wait for first reset do be done
    while flag loop
       wait until rising_edge(MGT_CKL_I);
       count:= count + 1;
 
       --arbitrary 
       if count > 410000 then
          assert false report "out of time" severity failure;
       end if;
  
       if TX_ANALOG = '0'  and TX_DIGITAL = '0' and TX_READY = '1' then
          flag <= false;
          count:= 0;
       end if;
    end loop;
  
    report "------------    MGT RESET TX succes   ---------------";
  
    flag <= true;
    wait until rising_edge(TX_FRAMECLK_I);
  
    --reset 2 and 3
    while flag loop
       wait until rising_edge(TX_FRAMECLK_I);
       count:= count + 1;
  
       if count > 410000 then --verify duration
          assert false report "out of time" severity failure;
       end if;
  
       if RX_ANALOG = '0'  and RX_DIGITAL = '0' and RX_READY = '1' then
          flag <= false;
          count:= 0;
       end if;
    end loop;
  
    report "------------  MGT RX RESET succes   ---------------";
  
    flag <= true;
    wait until rising_edge(TX_FRAMECLK_I);
  
    --reset 2 and 3
    while flag loop
       wait until rising_edge(TX_FRAMECLK_I);
       count:= count + 1;
  
       if count > 410000 then --verify duration
          assert false report "out of time" severity failure;
       end if;
  
       if TX_CLKEN_I = '1'  then
          flag <= false;
          count:= 0;
       end if;
    end loop;
  
    report "-------------  GBT TX RESET succes   ---------------";
  
    flag <= true;
    wait until rising_edge(RX_FRAMECLK_I);
  
    --reset 4
    while flag loop
       wait until rising_edge(RX_FRAMECLK_I);
       count:= count + 1;
  
       if count > 410000 then --verify duration
          assert false report "out of time" severity failure;
       end if;
  
       --Before the MGT_RX_RSTDONE
       if RX_CLKEN_I = '1'  and DONE_FROM_RXBITSLIPCONTROL = '1' then
          flag <= false;
          count:= 0;
       end if;
    end loop;
  
  
    report "--------------  GBT RX RESET succes  ---------------";
    -- End and close simulation
    wait;
    std.env.finish;
 
end process;

end behavioral;
