library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;

-- Custom libraries and packages:
use work.gbt_bank_package.all;
use work.vendor_specific_gbt_bank_package.all;
use work.gbt_exampledesign_package.all;

entity ttc_injector_tb is
   generic(
        NUM_LINKS                 : integer := 1;
        PATTERN_SEL               : std_logic_vector(1 downto 0):="10";
        CLOCKING_SCHEME           : integer range 0 to 1 := BC_CLOCK

);
end ttc_injector_tb;

architecture behavior of ttc_injector_tb is

  signal mgt_clk_s                   : std_logic;  --is a port
  signal gbt_isdataflag_rx_s         : std_logic_vector(1 to NUM_LINKS);  --rx data is valid
  signal gbt_isdataflag_tx_s         : std_logic_vector(1 to NUM_LINKS);  --tx data is valid
  signal gbt_errordetected_s         : std_logic_vector(1 to NUM_LINKS);
  signal gbt_errorflag_s             : gbt_reg84_A(1 to NUM_LINKS);
  signal gbt_txdata_s                : gbt_reg84_A(1 to NUM_LINKS);
  signal gbt_rxdata_s                : gbt_reg84_A(1 to NUM_LINKS);
  signal wb_txdata_s                 : gbt_reg32_A (1 to NUM_LINKS);
  signal wb_rxdata_s                 : gbt_reg32_A (1 to NUM_LINKS);
  signal frameclk_40mhz              : std_logic;  --from outside
  signal gbt_general_reset_s         : std_logic;
  signal gbtbank_rx_frameclk_ready_s : std_logic_vector(1 to NUM_LINKS);
  signal gbt_rxframeclk_s            : std_logic_vector(1 to NUM_LINKS);
  signal gbt_txframeclk_s            : std_logic_vector(1 to NUM_LINKS);
  signal mgt_txwordclk_s             : std_logic_vector(1 to NUM_LINKS);
  signal mgt_rxwordclk_s             : std_logic_vector(1 to NUM_LINKS);
  signal mgt_headerflag_s            : std_logic_vector(1 to NUM_LINKS);
  signal gbt_link_tx_ready_s         : std_logic_vector(1 to NUM_LINKS);
  signal gbt_link_rx_ready_s         : std_logic_vector(1 to NUM_LINKS);
  signal gbt_gbtrx_ready_s           : std_logic_vector(1 to NUM_LINKS);
  signal gbt_link_ready_s            : std_logic_vector(1 to NUM_LINKS);
  signal gbt_gbttx_ready_s           : std_logic_vector(1 to NUM_LINKS);
  signal gbt_mgt_rx                  : std_logic_vector(1 to NUM_LINKS);
  signal gbt_mgt_tx                  : std_logic_vector(1 to NUM_LINKS);
  signal gbt_rxclken_s               : std_logic_vector(1 to NUM_LINKS);
  signal gbt_txclken_s               : std_logic_vector(1 to NUM_LINKS);
  signal gbt_tx_reset_s              : std_logic_vector(1 to NUM_LINKS);
  signal gbt_rxready_s               : std_logic_vector(1 to NUM_LINKS);
  signal gbt_rxclkenlogic_s          : std_logic_vector(1 to NUM_LINKS);
  signal pattern_gen_rst             : std_logic_vector(1 to NUM_LINKS);
  signal no_valid_data               : std_logic_vector(1 to NUM_LINKS);
  signal reset_after_250us           : std_logic;

  signal tx_analog_s_bind                 : std_logic_vector(1 to NUM_LINKS); 
  signal tx_digital_s_bind                : std_logic_vector(1 to NUM_LINKS);
  signal tx_ready_s_bind                  : std_logic_vector(1 to NUM_LINKS);
  signal rx_analog_s_bind                 : std_logic_vector(1 to NUM_LINKS); 
  signal rx_digital_s_bind                : std_logic_vector(1 to NUM_LINKS); 
  signal rx_ready_s_bind                  : std_logic_vector(1 to NUM_LINKS);
  signal done_from_rxbitslipcontrol_s_bind: std_logic_vector(1 to NUM_LINKS);


  signal mgt_tx_reset_s_bind         : std_logic_vector(1 to NUM_LINKS);
  signal mgt_rx_reset_s_bind         : std_logic_vector(1 to NUM_LINKS);
  signal gbt_tx_reset_s_bind         : std_logic_vector(1 to NUM_LINKS);
  signal gbt_rx_reset_s_bind         : std_logic_vector(1 to NUM_LINKS);
  signal gbt_tx_rstdone_s_bind       : std_logic_vector(1 to NUM_LINKS);
  signal gbt_rx_rstdone_s_bind       : std_logic_vector(1 to NUM_LINKS);

begin

    -- Data pattern generator
    dataGenEn_output_gen : for i in 1 to NUM_LINKS generate

    pattern_gen_rst(i) <= gbt_tx_reset_s(i) or reset_after_250us or no_valid_data(i);

        gbtBank2_pattGen : entity work.gbt_pattern_generator
            generic map(
                CLOCKING_SCHEME => CLOCKING_SCHEME
            )
            port map (
                GENERAL_RST_I                       => gbt_general_reset_s --Connect to general resert -- GBTBANK_GENERAL_RESET_I or GBTBANK_MANUAL_RESET_TX_I,
                ,RESET_I                            => pattern_gen_rst(i)  --Gbt_tx_reset_s(i)       --Connect to gbt_tx_reset
                ,TX_FRAMECLK_I                      => frameclk_40mhz      --To 40Mhz -- FRAMECLK_40MHZ,
                ,TX_WORDCLK_I                       => mgt_txwordclk_s(i)  --(comes from gbt_bank) --mgt_txwordclk_s(i),
                ,TX_FRAMECLK_O                      => gbt_txframeclk_s(i) --Connect gbt_tx_frameclk_i from gbt_bank --gbt_txframeclk_s(i),
                ,TX_CLKEN_o                         => gbt_txclken_s(i)    --Connect to gbt_tx_clken_i from gbt_bank --gbt_txclken_s(i),
                ,TX_ENCODING_SEL_I                  => "00"
                ,TEST_PATTERN_SEL_I                 => PATTERN_SEL
                ,STATIC_PATTERN_SCEC_I              => "00"
                ,STATIC_PATTERN_DATA_I              => x"000BABEAC1DACDCFFFFF"
                ,STATIC_PATTERN_EXTRADATA_WIDEBUS_I => x"BEEFCAFE"
                ,TX_DATA_O                          => gbt_txdata_s(i)    --To the gbt_data ports
                ,TX_EXTRA_DATA_WIDEBUS_O            => wb_txdata_s(i)     --To the gbt_data ports
            );

    end generate;

    -- Data pattern checker
    gbtBank_patCheck_gen : for i in 1 to NUM_LINKS generate
        gbtBank_pattCheck : entity work.gbt_pattern_checker
            port map (
                RESET_I                               => gbt_general_reset_s    -- Connet to global reset
                ,RX_FRAMECLK_I                        => gbt_rxframeclk_s(i)    -- Connect to gbt_rxframeclk from phase align
                ,RX_CLKEN_I                           => gbt_rxclkenlogic_s(i)
                ,RX_DATA_I                            => gbt_rxdata_s(i)        -- gbt_rxdata_s(i),
                ,RX_EXTRA_DATA_WIDEBUS_I              => wb_rxdata_s(i)         -- wb_rxdata_s(i),
                ,GBT_RX_READY_I                       => gbt_rxready_s(i)       -- gbt_rxready_s(i),
                ,RX_ENCODING_SEL_I                    => "00"
                ,TEST_PATTERN_SEL_I                   => PATTERN_SEL
                ,STATIC_PATTERN_SCEC_I                => "00"
                ,STATIC_PATTERN_DATA_I                => x"000BABEAC1DACDCFFFFF"
                ,STATIC_PATTERN_EXTRADATA_WIDEBUS_I   => x"BEEFCAFE"
                ,RESET_GBTRXREADY_LOST_FLAG_I         =>'0'                     --(others =>'0') -- Both signals useul when ISSP used 
                ,RESET_DATA_ERRORSEEN_FLAG_I          =>'0'                     --(othres =>'0') -- idem 
                ,GBTRXREADY_LOST_FLAG_O               => open -- 
                ,RXDATA_ERRORSEEN_FLAG_O              => open -- 
                ,RXEXTRADATA_WIDEBUS_ERRORSEEN_FLAG_O => open -- 
            );
    end generate;

    --Instance of top design of gbt-bank
    inst_alt_sx_gbt_design : entity work.alt_sx_gbt_design
        generic map(
           NUM_LINKS => NUM_LINKS
        )
        port map (
            MGT_CLK_i                    => mgt_clk_s       
            ,GBT_ISDATAFLAG_i            => gbt_isdataflag_tx_s 
            ,GBT_ISDATAFLAG_o            => gbt_isdataflag_rx_s 
            ,GBT_ERRORDETECTED_o         => gbt_errordetected_s 
            ,GBT_ERRORFLAG_o             => gbt_errorflag_s
            ,GBT_TXDATA_i                => gbt_txdata_s
            ,GBT_RXDATA_o                => gbt_rxdata_s
            ,WB_TXDATA_i                 => wb_txdata_s 
            ,WB_RXDATA_o                 => wb_rxdata_s 
            ,FRAMECLK_40MHZ              => frameclk_40mhz  
            ,GBT_GENERAL_RESET_I         => gbt_general_reset_s 
            ,GBTBANK_RX_FRAMECLK_READY_O => gbtbank_rx_frameclk_ready_s  
            ,GBT_RXFRAMECLK_O            => gbt_rxframeclk_s
            ,GBT_TXFRAMECLK_i            => gbt_txframeclk_s
            ,MGT_TXWORDCLK_O             => mgt_txwordclk_s 
            ,MGT_RXWORDCLK_O             => mgt_rxwordclk_s 
            ,MGT_HEADERFLAG_O            => mgt_headerflag_s
            ,GBT_LINK_TX_READY_O         => gbt_link_tx_ready_s 
            ,GBT_LINK_RX_READY_O         => gbt_link_rx_ready_s 
            ,GBT_GBTRX_READY_O           => gbt_gbtrx_ready_s
            ,GBT_LINK_READY_O            => gbt_link_ready_s 
            ,GBT_GBTTX_READY_O           => gbt_gbttx_ready_s
            ,GBT_MGT_RX                  => gbt_mgt_rx 
            ,GBT_MGT_TX                  => gbt_mgt_tx 
            ,GBT_RXCLKEN_o               => gbt_rxclken_s
            ,GBT_RXREADY_o               => gbt_rxready_s
            ,GBT_RXCLKENLOGIC_o          => gbt_rxclkenlogic_s
            ,GBT_TXCLKEn_i              => gbt_txclken_s
            ,GBT_BANK_LOOPBACK              => '0'
        );

  gbt_mgt_rx     <= gbt_mgt_tx;         
  gbt_tx_reset_s <= not gbt_gbttx_ready_s;
  
  --acces to internal signals vhdl-2008  
  mgt_tx_reset_s_bind(1)   <= <<signal .ttc_injector_tb.inst_alt_sx_gbt_design.gbtBank_rst_gen(1).gbtBank_gbtBankRst.MGT_TX_RESET_O :std_logic>>;
  mgt_rx_reset_s_bind(1)   <= <<signal .ttc_injector_tb.inst_alt_sx_gbt_design.gbtBank_rst_gen(1).gbtBank_gbtBankRst.MGT_RX_RESET_O :std_logic>>;
  gbt_tx_reset_s_bind(1)   <= <<signal .ttc_injector_tb.inst_alt_sx_gbt_design.gbtBank_rst_gen(1).gbtBank_gbtBankRst.GBT_TX_RESET_O :std_logic>>;
  gbt_rx_reset_s_bind(1)   <= <<signal .ttc_injector_tb.inst_alt_sx_gbt_design.gbtBank_rst_gen(1).gbtBank_gbtBankRst.GBT_RX_RESET_O :std_logic>>;
  gbt_rx_rstdone_s_bind(1) <= <<signal .ttc_injector_tb.inst_alt_sx_gbt_design.gbtBank_rst_gen(1).gbtBank_gbtBankRst.MGT_RX_RSTDONE_I :std_logic>>;
  tx_analog_s_bind(1)      <= <<signal .ttc_injector_tb.inst_alt_sx_gbt_design.gbt_inst.mgt_inst.gxRstCtrl_gen(1).gxRstCtrl.TX_ANALOGRESET_O :std_logic>>;
  tx_digital_s_bind(1)     <= <<signal .ttc_injector_tb.inst_alt_sx_gbt_design.gbt_inst.mgt_inst.gxRstCtrl_gen(1).gxRstCtrl.TX_DIGITALRESET_O:std_logic>>;
  tx_ready_s_bind(1)       <= <<signal .ttc_injector_tb.inst_alt_sx_gbt_design.gbt_inst.mgt_inst.gxRstCtrl_gen(1).gxRstCtrl.TX_READY_O       :std_logic>>;
  rx_analog_s_bind(1)      <= <<signal .ttc_injector_tb.inst_alt_sx_gbt_design.gbt_inst.mgt_inst.gxRstCtrl_gen(1).gxRstCtrl.RX_ANALOGRESET_O :std_logic>>;
  rx_digital_s_bind(1)     <= <<signal .ttc_injector_tb.inst_alt_sx_gbt_design.gbt_inst.mgt_inst.gxRstCtrl_gen(1).gxRstCtrl.RX_DIGITALRESET_O:std_logic>>;
  rx_ready_s_bind(1)       <= <<signal .ttc_injector_tb.inst_alt_sx_gbt_design.gbt_inst.mgt_inst.gxRstCtrl_gen(1).gxRstCtrl.RX_READY_O       :std_logic>>;

  -- Signal also used to generate the rx_ready signal 
  done_from_rxbitslipcontrol_s_bind<= <<signal .ttc_injector_tb.inst_alt_sx_gbt_design.gbt_inst.mgt_inst.done_from_rxBitSlipControl  :std_logic_vector>>;

  -- Check the reset sequency. Allows to know if the Stratix tranceiver were well configured
  -- Some important changes between Stratix 10 an altera 10 can be seen with the reset behaviour
  rst_checker : for i in 1 to NUM_LINKS generate
    inst_ttc_injector_rst_checker: entity work.ttc_injector_rst_checker 
      generic map(
        NUM_LINKS => NUM_LINKS
      )
      port map(
         MGT_TXRESET_I              => mgt_tx_reset_s_bind(i) 
        ,MGT_RXRESET_I              => mgt_rx_reset_s_bind(i)
        ,GBT_TXRESET_I              => gbt_tx_reset_s_bind(i) 
        ,GBT_RXRESET_I              => gbt_rx_reset_s_bind(i)
        ,MGT_CKL_I                  => mgt_clk_s 
        ,TX_FRAMECLK_I              => frameclk_40mhz 
        ,RX_FRAMECLK_I              => gbt_rxframeclk_s(i)
	,TX_ANALOG                  => tx_analog_s_bind(i)                  
	,TX_DIGITAL                 => tx_digital_s_bind(i)                
	,TX_READY                   => tx_ready_s_bind(i)                  
	,RX_ANALOG                  => rx_analog_s_bind(i)                  
	,RX_DIGITAL                 => rx_digital_s_bind(i)                 
	,RX_READY                   => rx_ready_s_bind(i)                  
	,DONE_FROM_RXBITSLIPCONTROL => done_from_rxbitslipcontrol_s_bind(i)
        ,RX_CLKEN_I                 => gbt_rxclkenlogic_s(i)
        ,TX_CLKEN_I                 => gbt_txclken_s(i)

      );
  end generate;

  -- Simple clock generator
  proc_240mhz: process
  begin
    mgt_clk_s <= '0';
    wait for 2.0833 ns;
    mgt_clk_s <= '1';
    wait for 2.0833 ns;
  end process;
  
  -- Simple clock generator
  proc_40mhz: process
  begin
    frameclk_40mhz<= '0';
    wait for 12.5 ns;
    frameclk_40mhz<= '1';
    wait for 12.5 ns;
  end process;
  
  -- Simple reset generator
  proc_reset: process
  begin
    gbt_general_reset_s <= '1';
    wait for 1000 ns;
    gbt_general_reset_s <= '0';
    wait;
  end process;
  
  --Signal to reset data generator 
  proc_reset_after_250us: process
  begin
    reset_after_250us <= '0';
    wait for 250 us;
    reset_after_250us <= '1';
    wait for 1000 ns;
    reset_after_250us <= '0';
    wait;
  end process;
 
  -- Play with the data valid from data generator. see behaviour 
--  proc_valid_data: process
--  begin
--    no_valid_data       <= (others => '1');
--    gbt_isdataflag_tx_s <= (others => '0');
--    wait until gbt_rxready_s= "11";
--    no_valid_data       <= (others => '0');
--    gbt_isdataflag_tx_s <= (others => '1');
--    wait for 50 us;
--    no_valid_data       <= (others => '1');
--    gbt_isdataflag_tx_s <= (others => '0');
--    wait;
--  end process;

end behavior;
