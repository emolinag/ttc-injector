
add wave -position insertpoint  \
sim:/ttc_injector_tb/inst_alt_sx_gbt_design/gbt_inst/mgt_inst/gxLatOpt_x1_gen/gxLatOpt_x1/* \
sim:/ttc_injector_tb/inst_alt_sx_gbt_design/gbt_inst/*

##add wave -divider "gbt bank reset"  -position insertpoint  -group "gbt bank reset" [\ 
add wave -position insertpoint  \
sim:/ttc_injector_tb/inst_alt_sx_gbt_design/gbtBank_rst_gen(1)/gbtBank_gbtBankRst/INITIAL_DELAY \
sim:/ttc_injector_tb/inst_alt_sx_gbt_design/gbtBank_rst_gen(1)/gbtBank_gbtBankRst/GBT_CLK_I \
sim:/ttc_injector_tb/inst_alt_sx_gbt_design/gbtBank_rst_gen(1)/gbtBank_gbtBankRst/TX_FRAMECLK_I \
sim:/ttc_injector_tb/inst_alt_sx_gbt_design/gbtBank_rst_gen(1)/gbtBank_gbtBankRst/RX_FRAMECLK_I \
sim:/ttc_injector_tb/inst_alt_sx_gbt_design/gbtBank_rst_gen(1)/gbtBank_gbtBankRst/TX_CLKEN_I \
sim:/ttc_injector_tb/inst_alt_sx_gbt_design/gbtBank_rst_gen(1)/gbtBank_gbtBankRst/RX_CLKEN_I \
sim:/ttc_injector_tb/inst_alt_sx_gbt_design/gbtBank_rst_gen(1)/gbtBank_gbtBankRst/MGTCLK_I \
sim:/ttc_injector_tb/inst_alt_sx_gbt_design/gbtBank_rst_gen(1)/gbtBank_gbtBankRst/GENERAL_RESET_I \
sim:/ttc_injector_tb/inst_alt_sx_gbt_design/gbtBank_rst_gen(1)/gbtBank_gbtBankRst/TX_RESET_I \
sim:/ttc_injector_tb/inst_alt_sx_gbt_design/gbtBank_rst_gen(1)/gbtBank_gbtBankRst/RX_RESET_I \
sim:/ttc_injector_tb/inst_alt_sx_gbt_design/gbtBank_rst_gen(1)/gbtBank_gbtBankRst/MGT_TX_RESET_O \
sim:/ttc_injector_tb/inst_alt_sx_gbt_design/gbtBank_rst_gen(1)/gbtBank_gbtBankRst/MGT_RX_RESET_O \
sim:/ttc_injector_tb/inst_alt_sx_gbt_design/gbtBank_rst_gen(1)/gbtBank_gbtBankRst/GBT_TX_RESET_O \
sim:/ttc_injector_tb/inst_alt_sx_gbt_design/gbtBank_rst_gen(1)/gbtBank_gbtBankRst/GBT_RX_RESET_O \
sim:/ttc_injector_tb/inst_alt_sx_gbt_design/gbtBank_rst_gen(1)/gbtBank_gbtBankRst/MGT_TX_RSTDONE_I \
sim:/ttc_injector_tb/inst_alt_sx_gbt_design/gbtBank_rst_gen(1)/gbtBank_gbtBankRst/MGT_RX_RSTDONE_I \
sim:/ttc_injector_tb/inst_alt_sx_gbt_design/gbtBank_rst_gen(1)/gbtBank_gbtBankRst/genReset_s \
sim:/ttc_injector_tb/inst_alt_sx_gbt_design/gbtBank_rst_gen(1)/gbtBank_gbtBankRst/mgtTxReset_s \
sim:/ttc_injector_tb/inst_alt_sx_gbt_design/gbtBank_rst_gen(1)/gbtBank_gbtBankRst/mgtRxReset_s \
sim:/ttc_injector_tb/inst_alt_sx_gbt_design/gbtBank_rst_gen(1)/gbtBank_gbtBankRst/gbtTxReset_s \
sim:/ttc_injector_tb/inst_alt_sx_gbt_design/gbtBank_rst_gen(1)/gbtBank_gbtBankRst/gbtRxReset_s \
sim:/ttc_injector_tb/inst_alt_sx_gbt_design/gbtBank_rst_gen(1)/gbtBank_gbtBankRst/mgtRxReady_s \
sim:/ttc_injector_tb/inst_alt_sx_gbt_design/gbtBank_rst_gen(1)/gbtBank_gbtBankRst/mgtRxReady_sync_s \
sim:/ttc_injector_tb/inst_alt_sx_gbt_design/gbtBank_rst_gen(1)/gbtBank_gbtBankRst/mgtTxReady_s \
sim:/ttc_injector_tb/inst_alt_sx_gbt_design/gbtBank_rst_gen(1)/gbtBank_gbtBankRst/mgtTxReady_sync_s \
sim:/ttc_injector_tb/inst_alt_sx_gbt_design/gbtBank_rst_gen(1)/gbtBank_gbtBankRst/genRstMgtClk_s \
sim:/ttc_injector_tb/inst_alt_sx_gbt_design/gbtBank_rst_gen(1)/gbtBank_gbtBankRst/genRstMgtClk_sync_s \
sim:/ttc_injector_tb/inst_alt_sx_gbt_design/gbtBank_rst_gen(1)/gbtBank_gbtBankRst/genTxRstMgtClk_s \
sim:/ttc_injector_tb/inst_alt_sx_gbt_design/gbtBank_rst_gen(1)/gbtBank_gbtBankRst/genTxRstMgtClk_sync_s \
sim:/ttc_injector_tb/inst_alt_sx_gbt_design/gbtBank_rst_gen(1)/gbtBank_gbtBankRst/genRxRstMgtClk_s \
sim:/ttc_injector_tb/inst_alt_sx_gbt_design/gbtBank_rst_gen(1)/gbtBank_gbtBankRst/genRxRstMgtClk_sync_s

add wave -position insertpoint  \
sim:/ttc_injector_tb/inst_alt_sx_gbt_design/gbt_inst/mgt_inst/gxRstCtrl_gen(1)/gxRstCtrl/stratix10Reset/gxResetTx/*

add wave -position insertpoint \
sim:/ttc_injector_tb/inst_alt_sx_gbt_design/gbt_inst/mgt_inst/gxRstCtrl_gen(1)/gxRstCtrl/stratix10Reset/gxResetRx/*

##add wave -position insertpoint \
##sim:/ttc_injector_tb/inst_alt_sx_gbt_design/gbt_inst/mgt_inst/gxLatOpt_x2_gen/gxLatOpt_x2/*


add wave -position insertpoint  \
sim:/ttc_injector_tb/inst_alt_sx_gbt_design/gbt_inst/mgt_inst/rxReady_from_gxRstCtrl

add wave -position insertpoint  \
sim:/ttc_injector_tb/inst_alt_sx_gbt_design/gbt_inst/mgt_inst/done_from_rxBitSlipControl

add wave -position insertpoint sim:/ttc_injector_tb/inst_alt_sx_gbt_design/gbt_inst/mgt_inst/rxPhaseAlign_numLinks_gen(1)/rxBitSlipControl/*


add wave -position insertpoint sim:/ttc_injector_tb/inst_alt_sx_gbt_design/gbt_inst/gbt_rxdatapath_multilink_gen(1)/gbt_rxdatapath_inst/*
add wave -position insertpoint sim:/ttc_injector_tb/inst_alt_sx_gbt_design/gbt_inst/gbt_rxdatapath_multilink_gen(1)/gbt_rxdatapath_inst/decoder/*
add wave -position insertpoint sim:/ttc_injector_tb/inst_alt_sx_gbt_design/gbt_inst/gbt_rxdatapath_multilink_gen(1)/gbt_rxdatapath_inst/descrambler/*
