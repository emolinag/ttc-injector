
set sources "./src"
set sources_tb "./sim"

do gbt_bank.do

vcom -2008 -work work \
$sources/example_designs/core_sources/exampleDsgn_package.vhd\

do phase_align.do

vcom -2008 -work work \
$sources/gbt-fpga/example_designs/core_sources/gbt_bank_reset.vhd \
$sources/example_designs/altera_s10/core_sources/alt_sx_gbt_design.vhd

##add here for test bench => pattern gen, pattern check, top
vcom -2008 -work work \
$sources/example_designs/core_sources/gbt_pattern_generator.vhd\
$sources/gbt-fpga/example_designs/core_sources/gbt_pattern_checker.vhd

vcom -2008 -work work \
$sources_tb/ttc_injector_rst_checker.vhd\
$sources_tb/ttc_injector_s10_tb.vhd

quit
